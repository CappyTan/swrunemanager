﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Text;

namespace WellPoint.Marketing.CampaignTracker.Framework.Extensions
{
    public static class EnumerationExtensions
    {
        #region Public Methods

        /// <summary>
        ///     Converts a sting into an element of the specified enum
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="enumString"></param>
        /// <returns></returns>
        public static T ConvertStringToEnum<T>(string enumString)
        {
            try
            {
                return (T)Enum.Parse(typeof(T), enumString, true);
            }
            catch (Exception ex)
            {
                string s = string.Format("'{0}' is not a valid enumeration of '{1}'", enumString, typeof(T).Name);
                throw new Exception(s, ex);
            }
        }

        public static bool TryConvertStringToEnum<T>(string enumString)
        {
            try
            {
                var enumValue = (T)Enum.Parse(typeof(T), enumString, true);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public static List<T> EnumToList<T>()
        {
            Type enumType = typeof(T);

            if (enumType.BaseType != typeof(Enum))
            {
                throw new ArgumentException("T must be of type System.Enum");
            }

            return new List<T>(Enum.GetValues(enumType) as IList<T>); // as IEnumerable<T>);
        }

        public static string GetDescription(this Enum value)
        {
            if (value == null)
            {
                return null;
            }

            Type type = value.GetType();
            string name = Enum.GetName(type, value);
            if (name != null)
            {
                FieldInfo field = type.GetField(name);
                if (field != null)
                {
                    var attr = Attribute.GetCustomAttribute(field, typeof(DescriptionAttribute)) as DescriptionAttribute;
                    if (attr != null)
                    {
                        return attr.Description;
                    }
                }
            }
            return name;
        }

        #endregion
    }
}
