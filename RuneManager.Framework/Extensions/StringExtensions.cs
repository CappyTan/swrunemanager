﻿namespace WellPoint.Marketing.CampaignTracker.Framework.Extensions
{
    public static class StringExtensions
    {
        public static string SQLify(this string field)
        {
            if (field == null)
            {
                return string.Empty;
            }

            return field.Replace("'", "''");
        }
    }
}
