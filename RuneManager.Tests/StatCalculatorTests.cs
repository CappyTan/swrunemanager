﻿namespace RuneManager.Tests
{
    using System.Collections.Generic;

    using Microsoft.VisualStudio.TestTools.UnitTesting;

    using RuneManager.Domain.Base;
    using RuneManager.Domain.Models;
    using RuneManager.Domain.Models.ValueObjects;
    using RuneManager.Services.Calculators;

    [TestClass]
    public class StatCalculatorTests
    {
        [TestMethod]
        public void WhenGetEffectiveHealthIsCalled()
        {
            var statCalculator = new StatCalculator();

            var monster = new Monster { HitPoints = 10000, Defense = 1000 };

            var health = statCalculator.GetEffectiveHealth(monster, new List<Rune>());

            Assert.AreEqual(50032, health);
        }

        [TestMethod]
        public void WhenGetEffectiveHealthIsCalledWithOptimizedStats()
        {
            var statCalculator = new StatCalculator();

            var monster = new Monster { HitPoints = 10000, Defense = 1000 };

            var health = statCalculator.GetEffectiveHealth(monster, new OptimizedStats { TotalDefense = 1000, HitPoints = 10000 });

            Assert.AreEqual(50032, health);
        }
    }
}
