﻿namespace RuneManager.Tests
{
    using System.Collections.Generic;
    using System.Linq;

    using Microsoft.VisualStudio.TestTools.UnitTesting;

    using RuneManager.Domain.Enumerations;
    using RuneManager.Domain.Models;
    using RuneManager.Domain.Models.ValueObjects;
    using RuneManager.Services.Optimizers;
    using RuneManager.Services.Optimizers.Attack;

    [TestClass]
    public class OptimizerTests
    {
        private IList<Rune> runes;

        [TestInitialize]
        public void Initialize()
        {
            runes = new List<Rune>
                            {
                                new Rune { Type = SetType.Focus, Slot = 1, MainStat = new Stat { Type = StatType.ATK, Value = 160}, BonusStat = new Stat { Type = StatType.ATKPercentage, Value = 8}, Stat1 = new Stat { Type = StatType.CD,  Value = 15}, Stat2 = new Stat
                                                                                                                                                                                                                                                               {
                                                                                                                                                                                                                                                                   Type = StatType.CR, Value = 15
                                                                                                                                                                                                                                                               }},
                                new Rune { Type = SetType.Focus, Slot = 2, MainStat = new Stat { Type = StatType.ATKPercentage, Value = 63}, BonusStat = new Stat { Type = StatType.HPPercentage, Value = 8}, Stat1 = new Stat { Type = StatType.CD,  Value = 15}, Stat2 = new Stat
                                                                                                                                                                                                                                                               {
                                                                                                                                                                                                                                                                   Type = StatType.CR, Value = 15
                                                                                                                                                                                                                                                               }},
                                new Rune { Type = SetType.Fatal, Slot = 3, MainStat = new Stat { Type = StatType.DEF, Value = 160}, BonusStat = new Stat { Type = StatType.ATKPercentage, Value = 8}, Stat1 = new Stat { Type = StatType.CD,  Value = 15}, Stat2 = new Stat
                                                                                                                                                                                                                                                               {
                                                                                                                                                                                                                                                                   Type = StatType.CR, Value = 15
                                                                                                                                                                                                                                                               }},
                                new Rune { Type = SetType.Fatal, Slot = 4, MainStat = new Stat { Type = StatType.CD, Value = 80}, BonusStat = new Stat { Type = StatType.ATKPercentage, Value = 8}, Stat1 = new Stat { Type = StatType.CD,  Value = 15}, Stat2 = new Stat
                                                                                                                                                                                                                                                               {
                                                                                                                                                                                                                                                                   Type = StatType.CR, Value = 15
                                                                                                                                                                                                                                                               }},
                                new Rune { Type = SetType.Fatal, Slot = 5, MainStat = new Stat { Type = StatType.HP, Value = 2488}, BonusStat = new Stat { Type = StatType.ATKPercentage, Value = 8}, Stat1 = new Stat { Type = StatType.CD,  Value = 15}, Stat2 = new Stat
                                                                                                                                                                                                                                                               {
                                                                                                                                                                                                                                                                   Type = StatType.CR, Value = 15
                                                                                                                                                                                                                                                               }},
                                new Rune { Type = SetType.Fatal, Slot = 6, MainStat = new Stat { Type = StatType.ATKPercentage, Value = 63}, BonusStat = new Stat { Type = StatType.DEFPercentage, Value = 8}, Stat1 = new Stat { Type = StatType.CD,  Value = 15}, Stat2 = new Stat
                                                                                                                                                                                                                                                               {
                                                                                                                                                                                                                                                                   Type = StatType.CR, Value = 15
                                                                                                                                                                                                                                                               }},
                            };
        }

        [TestMethod]
        public void NoRequiredSetsSucceeds()
        {
            var monster = new Monster { Speed = 100, CritChance = 15, Attack = 900 };

            var maxNukeFinder = new MaxCritOptimizer();
            var runeSets = maxNukeFinder.FindSets(monster, runes, new OptimizerParameters(), null);


            Assert.AreEqual(1, runeSets.ToList().Count);
        }
            
        [TestMethod]
        public void RequireWrongFourSetFails()
        {
            var monster = new Monster { Speed = 100, CritChance = 15, Attack = 900 };

            var maxNukeFinder = new MaxCritOptimizer();
            var runeSets = maxNukeFinder.FindSets(monster, runes, new OptimizerParameters { FourSet = SetType.Violent }, null);


            Assert.AreEqual(0, runeSets.ToList().Count);
        }

        [TestMethod]
        public void RequireCorrectFourSetSucceeds()
        {
            var monster = new Monster { Speed = 100, CritChance = 15, Attack = 900 };

            var maxNukeFinder = new MaxCritOptimizer();
            var runeSets = maxNukeFinder.FindSets(monster, runes, new OptimizerParameters { FourSet = SetType.Fatal });


            Assert.AreEqual(1, runeSets.ToList().Count);
        }

        [TestMethod]
        public void RequireWrongTwoSetFails()
        {
            var monster = new Monster { Speed = 100, CritChance = 15, Attack = 900 };

            var maxNukeFinder = new MaxCritOptimizer();
            var runeSets = maxNukeFinder.FindSets(monster, runes, new OptimizerParameters { TwoSet = SetType.Blade });


            Assert.AreEqual(0, runeSets.ToList().Count);
        }

        [TestMethod]
        public void RequireCorrectTwoSetSucceeds()
        {
            var monster = new Monster { Speed = 100, CritChance = 15, Attack = 900 };

            var maxNukeFinder = new MaxCritOptimizer();
            var runeSets = maxNukeFinder.FindSets(monster, runes, new OptimizerParameters { TwoSet = SetType.Focus });


            Assert.AreEqual(1, runeSets.ToList().Count);
        }

        [TestMethod]
        public void RequireWongFourAndTwoSetsFails()
        {
            var monster = new Monster { Speed = 100, CritChance = 15, Attack = 900 };

            var maxNukeFinder = new MaxCritOptimizer();
            var runeSets = maxNukeFinder.FindSets(monster, runes, new OptimizerParameters { FourSet = SetType.Violent, TwoSet = SetType.Focus });


            Assert.AreEqual(0, runeSets.ToList().Count);
        }

        [TestMethod]
        public void RequireCorrectFourAndTwoSetsSucceeds()
        {
            var monster = new Monster { Speed = 100, CritChance = 15, Attack = 900 };

            var maxNukeFinder = new MaxCritOptimizer();
            var runeSets = maxNukeFinder.FindSets(monster, runes, new OptimizerParameters { FourSet = SetType.Fatal, TwoSet = SetType.Focus });


            Assert.AreEqual(1, runeSets.ToList().Count);
        }
    }
}
