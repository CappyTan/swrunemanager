﻿namespace RuneManager.Services
{
    using System.IO;
    using System.Linq;

    using CsvHelper;

    using RuneManager.Domain.Contracts.Repositories;
    using RuneManager.Domain.Contracts.Services;
    using RuneManager.Domain.Models;
    using RuneManager.Services.ExportMaps;

    public class MonsterService : IMonsterService
    {
        private readonly IMonsterRepository monsterRepository;

        public MonsterService(IMonsterRepository monsterRepository)
        {
            this.monsterRepository = monsterRepository;
        }

        public void AddMonster(Monster monster)
        {
            this.monsterRepository.Add(monster);
        }

        public string ExportPendingRunes()
        {
            var filename = "PendingRunes.csv";
            var stream = new FileStream(filename, FileMode.Create, FileAccess.Write);
            var writer = new StreamWriter(stream);

            var monsters = this.monsterRepository.GetMonsters().Where(p => p.PendingRunes.Any());


            var csv = new CsvWriter(writer);

            csv.WriteField("Name");
            csv.WriteField("Slot 1");
            csv.WriteField("Slot 2");
            csv.WriteField("Slot 3");
            csv.WriteField("Slot 4");
            csv.WriteField("Slot 5");
            csv.WriteField("Slot 6");

            csv.NextRecord();

            foreach (var monster in monsters)
            {
                csv.WriteField(monster.Name);
                csv.WriteField(this.GetCommaSeparatedStatsBySlot(monster, 1));
                csv.WriteField(this.GetCommaSeparatedStatsBySlot(monster, 2));
                csv.WriteField(this.GetCommaSeparatedStatsBySlot(monster, 3));
                csv.WriteField(this.GetCommaSeparatedStatsBySlot(monster, 4));
                csv.WriteField(this.GetCommaSeparatedStatsBySlot(monster, 5));
                csv.WriteField(this.GetCommaSeparatedStatsBySlot(monster, 6));

                csv.NextRecord();
            }

            //csv.Configuration.RegisterClassMap<PendingRuneExportMap>();
            //csv.WriteRecords(monsters);
            writer.Flush();
            writer.Close();

            return filename;
        }

        public void UpdateMonster(Monster monster)
        {
            this.monsterRepository.Update(monster);
        }

        private string GetCommaSeparatedStatsBySlot(Monster monster, int slot)
        {
            var rune = monster.PendingRunes.Single(p => p.Slot == slot);
            return string.Join(",", rune.Stats.Select(st => st.ReadableValue)) + "," + rune.Type + "," + (rune.Monster != null ? rune.Monster.Name : string.Empty);
        }
    }
}
