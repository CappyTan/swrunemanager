﻿namespace RuneManager.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using Newtonsoft.Json;

    using RuneManager.Domain;
    using RuneManager.Domain.Contracts.Repositories;
    using RuneManager.Domain.Contracts.Services;
    using RuneManager.Domain.Models;
    using RuneManager.Domain.Models.ValueObjects;

    public class RuneService : IRuneService
    {
        private readonly IRuneRepository runeRepository;

        private readonly IMonsterRepository monsterRepository;

        public RuneService(IRuneRepository runeRepository, IMonsterRepository monsterRepository)
        {
            this.runeRepository = runeRepository;
            this.monsterRepository = monsterRepository;
        }

        public void AddRune(Rune rune)
        {
            this.runeRepository.Add(rune);
        }

        public Rune GetRune(int id)
        {
            return this.runeRepository.Find(id);
        }

        public void UpdateRune(Rune rune)
        {
            this.runeRepository.Update(rune);
            rune.ClearStatCache();
        }

        public void SellRune(Rune rune)
        {
            this.runeRepository.Delete(rune);
        }

        public IList<Rune> ImportRunes(string importString, bool deleteExisting, bool useMaxLevelMainStats)
        {
            IList<Rune> errors = new List<Rune>();
            ImportedJSON json = JsonConvert.DeserializeObject<ImportedJSON>(importString);
            var monsters = this.monsterRepository.GetMonsters();

            if (deleteExisting)
            {
                var runes = this.runeRepository.GetRunes();
                foreach (var rune in runes)
                {
                    this.runeRepository.Delete(rune);
                }
            }

            foreach (var importedRune in json.runes)
            {
                try
                {
                    var rune = importedRune.ToRune(useMaxLevelMainStats, true);
                    this.SetMonster(importedRune, monsters, rune);
                    this.runeRepository.Add(rune);
                }
                catch
                {
                    var rune = importedRune.ToRune(useMaxLevelMainStats, false);
                    this.SetMonster(importedRune, monsters, rune);
                    errors.Add(rune);
                }
                
            }

            return errors;
        }

        private void SetMonster(ImportedRune importedRune, IList<Monster> monsters, Rune rune)
        {
            Monster monster = null;
            if (importedRune.monster_n.Contains(" (In Storage)"))
            {
                monster =
                    monsters.FirstOrDefault(
                        p => p.Name.ToUpper() == importedRune.monster_n.Replace(" (In Storage)", " 2").ToUpper());
            }

            if (monster == null)
            {
                monster =
                    monsters.FirstOrDefault(
                        p => p.Name.ToUpper() == importedRune.monster_n.Replace(" (In Storage)", string.Empty).ToUpper());
            }

            rune.Monster = monster;
        }
    }
}
