﻿using System;
using RuneManager.Domain.Contracts.Repositories;
using RuneManager.Domain.Models.ValueObjects;
using RuneManager.Services.Optimizers.Attack;

namespace RuneManager.Services.Optimizers
{
    public class RaidFrontLineDPSOptimizer : DamageOptimizerBase
    {
        public RaidFrontLineDPSOptimizer(IBuildingsRepository buildingsRepository) : base(buildingsRepository)
        {
        }

        public override string Description
        {
            get { return "Attack Based DPS placed in front line for raids. Increases EHP via Defense"; }
        }

        public override string Examples
        {
            get { return "KFG, Magical Archer"; }
        }

        protected override Func<RuneSet, double> OptimizedVariablePredicate()
        {
            throw new NotImplementedException();
        }
    }
}
