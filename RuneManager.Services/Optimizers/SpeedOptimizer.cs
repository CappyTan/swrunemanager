﻿namespace RuneManager.Services.Optimizers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using RuneManager.Domain.Base;
    using RuneManager.Domain.Contracts.Repositories;
    using RuneManager.Domain.Enumerations;
    using RuneManager.Domain.Models;
    using RuneManager.Domain.Models.ValueObjects;

    public class SpeedOptimizer : OptimizerBase
    {
        public override string Description
        {
            get
            {
                return
                    "Maximizes total speed at the expense of everything else";
            }
        }

        public override string Examples
        {
            get
            {
                return "Bernard, Chloe, Megan";
            }
        }

        protected override IList<Rune> FilterOutIrrelevantRunes(Monster monster, IList<Rune> runes, OptimizerSpeedType optimizerSpeed)
        {
            var slots = new[] { 1, 3, 4, 5, 6 };
            var mainStats = new List<StatType> { StatType.SPD };
            var desiredStats = new List<StatType> { StatType.SPD };
            var minStatThreshold = 8;

            if (parameters.MinHitPoints - monster.HitPoints > 0)
            {
                desiredStats.Add(StatType.HPPercentage);
                minStatThreshold += 5;
            }

            if (parameters.MinDefense - monster.Defense > 0)
            {
                desiredStats.Add(StatType.DEFPercentage);
                minStatThreshold += 5;
            }

            if (parameters.MinResistance - monster.Resistance > 0)
            {
                minStatThreshold += 5;
                desiredStats.Add(StatType.RES);
            }

            if (parameters.MinAccuracy - monster.Accuracy > 0)
            {
                desiredStats.Add(StatType.ACC);
                minStatThreshold += 5;
            }

            if (parameters.MinResistance - monster.Resistance > 0)
            {
                minStatThreshold += 5;
                desiredStats.Add(StatType.RES);
            }

            if (this.parameters.MinEffectiveHitPoints > 0)
            {
                mainStats.Add(StatType.HPPercentage);
                mainStats.Add(StatType.DEFPercentage);
                desiredStats.Add(StatType.DEFPercentage);
                desiredStats.Add(StatType.HPPercentage);
            }

            runes = runes.Where(p => p.TotalDesiredStats(desiredStats) > minStatThreshold).ToList();

            runes = this.FilterTop20PerSlot(runes, parameters, slots, mainStats, desiredStats);

            return runes;
        }

        protected override Func<RuneSet, double> OptimizedVariablePredicate()
        {
            return p => p.TotalSpeed;
        }

        public SpeedOptimizer(IBuildingsRepository buildingsRepository)
            : base(buildingsRepository)
        {
        }
    }
}
