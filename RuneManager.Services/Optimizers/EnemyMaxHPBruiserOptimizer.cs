﻿namespace RuneManager.Services.Optimizers
{
    using System;
    using System.Collections.Generic;

    using RuneManager.Domain.Base;
    using RuneManager.Domain.Contracts.Repositories;
    using RuneManager.Domain.Models;
    using RuneManager.Domain.Models.ValueObjects;
    using RuneManager.Services.Optimizers.HitPoint;

    public class EnemyMaxHPBruiserOptimizer : HitPointBruiserOptimizer
    {
        public override string Description
        {
            get
            {
                return
                    "A balanced build between damage over time and effective health for monsters whose primary damage scales with enemy max hit points.";
            }
        }

        public override string Examples
        {
            get
            {
                return "Spectra, Marble, Jamire";
            }
        }

        protected override Func<RuneSet, double> OptimizedVariablePredicate()
        {
            return p => p.CritCoefficient * p.EffectiveHealth;
        }

        public EnemyMaxHPBruiserOptimizer(IBuildingsRepository buildingsRepository)
            : base(buildingsRepository)
        {
        }
    }
}
