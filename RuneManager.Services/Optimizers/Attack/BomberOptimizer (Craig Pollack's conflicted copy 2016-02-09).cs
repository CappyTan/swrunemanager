﻿namespace RuneManager.Services.Optimizers.Attack
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using RuneManager.Domain.Base;
    using RuneManager.Domain.Contracts.Repositories;
    using RuneManager.Domain.Enumerations;
    using RuneManager.Domain.Models;
    using RuneManager.Domain.Models.ValueObjects;

    public class BomberOptimizer : OptimizerBase
    {
        protected override IList<Rune> FilterOutIrrelevantRunes(Monster monster, IList<Rune> runes, OptimizerSpeedType optimizerSpeed)
        {
            var slots = new[] { 1, 3, 5 };
            var mainStats = new List<StatType> { StatType.ATKPercentage };
            var desiredStats = new List<StatType> { StatType.ATKPercentage };

            var minStats = optimizerSpeed == OptimizerSpeedType.Fast ? 20 : 10;

            if (this.parameters.MinSpeed - monster.Speed >= 21)
            {
                mainStats.Add(StatType.SPD);
            }

            if (this.parameters.MinSpeed - monster.Speed > 0)
            {
                desiredStats.Add(StatType.SPD);
            }

            if (this.parameters.MinHitPoints >= 1.63 * monster.HitPoints)
            {
                mainStats.Add(StatType.HPPercentage);
                desiredStats.Add(StatType.HPPercentage);
            }

            if (this.parameters.MinAccuracy - monster.Accuracy >= 32)
            {
                mainStats.Add(StatType.ACC);
                desiredStats.Add(StatType.ACC);
            }

            if (this.parameters.MinResistance - monster.Resistance >= 32)
            {
                mainStats.Add(StatType.RES);
                desiredStats.Add(StatType.RES);
            }

            //runes = runes.Where(p => p.TotalDesiredStats(desiredStats) > 20).ToList();

            runes = this.FilterTop20PerSlot(runes, this.parameters, slots, mainStats, desiredStats);

            return runes;
        }

        public override string Description
        {
            get
            {
                return
                    "This will maximize bomb damage for any bomb using monster. It will not maximize their non bomb attacks.";
            }
        }

        public override string Examples
        {
            get
            {
                return "Malaka, Jojo, Taurus";
            }
        }

        protected override Func<RuneSet, double> OptimizedVariablePredicate()
        {
            return p => p.AttackCoefficient;
        }

        public BomberOptimizer(IBuildingsRepository buildingsRepository)
            : base(buildingsRepository)
        {
        }
    }
}
