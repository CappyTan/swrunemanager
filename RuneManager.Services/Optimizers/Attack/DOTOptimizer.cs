﻿namespace RuneManager.Services.Optimizers.Attack
{
    using System;

    using RuneManager.Domain.Contracts.Repositories;
    using RuneManager.Domain.Models.ValueObjects;

    public class DOTOptimizer : DamageOptimizerBase
    {
        public override string Description
        {
            get
            {
                return
                    "Maximizes average damage over time for monsters whose damage scales with attack. This is used for monsters who can not reach 100% crit rate and you want to optimzie their total damage output and don't need very specific speeds. Monsters in this category scale particularly well with violent runes.";
            }
        }

        public override string Examples
        {
            get
            {
                return "Theomars, Rakshasa, Occult Girls";
            }
        }

        protected override Func<RuneSet, double> OptimizedVariablePredicate()
        {
            return p => p.DOTMultiplier;
        }

        public DOTOptimizer(IBuildingsRepository buildingsRepository)
            : base(buildingsRepository)
        {
        }
    }
}
