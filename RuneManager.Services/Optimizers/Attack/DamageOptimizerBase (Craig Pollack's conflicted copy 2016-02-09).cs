﻿namespace RuneManager.Services.Optimizers.Attack
{
    using System.Collections.Generic;
    using System.Linq;

    using RuneManager.Domain.Base;
    using RuneManager.Domain.Contracts.Repositories;
    using RuneManager.Domain.Enumerations;
    using RuneManager.Domain.Models;

    public abstract class DamageOptimizerBase : OptimizerBase
    {
        protected override IList<Rune> FilterOutIrrelevantRunes(Monster monster, IList<Rune> runes, OptimizerSpeedType optimizerSpeed)
        {
            var slots = new[] { 1, 3, 5 };
            var mainStats = new List<StatType> { StatType.ATKPercentage, StatType.CR, StatType.CD, };
            var desiredStats = new List<StatType> { StatType.ATKPercentage, StatType.CR, StatType.CD };

            var minStats = optimizerSpeed == OptimizerSpeedType.Fast ? 20 : 10;

            if (this.parameters.MinSpeed - monster.Speed >= 21)
            {
                mainStats.Add(StatType.SPD);
            }

            if (this.parameters.MinHitPoints >= 1.63 * monster.HitPoints)
            {
                mainStats.Add(StatType.HPPercentage);
            }

            if (this.parameters.MinAccuracy - monster.Accuracy >= 32)
            {
                mainStats.Add(StatType.ACC);
                
            }

            if (this.parameters.MinResistance - monster.Resistance >= 32)
            {
                mainStats.Add(StatType.RES);
                
            }

            if (this.parameters.MinSpeed - monster.Speed > 0)
            {
                desiredStats.Add(StatType.SPD);
            }

            if (this.parameters.MinAccuracy - monster.Accuracy > 0)
            {
                desiredStats.Add(StatType.ACC);
            }

            if (this.parameters.MinResistance - monster.Resistance > 0)
            {
                desiredStats.Add(StatType.RES);
            }

            if (this.parameters.MinHitPoints > monster.HitPoints)
            {
                desiredStats.Add(StatType.HPPercentage);
            }

            runes = runes.Where(p => p.TotalDesiredStats(desiredStats) > minStats).ToList();

            runes = this.FilterTop20PerSlot(runes, this.parameters, slots, mainStats, desiredStats);

            return runes;
        }

        protected DamageOptimizerBase(IBuildingsRepository buildingsRepository)
            : base(buildingsRepository)
        {
        }
    }
}
