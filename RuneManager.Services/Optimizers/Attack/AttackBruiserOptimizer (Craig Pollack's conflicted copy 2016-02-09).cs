﻿namespace RuneManager.Services.Optimizers.Attack
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using RuneManager.Domain.Base;
    using RuneManager.Domain.Contracts.Repositories;
    using RuneManager.Domain.Enumerations;
    using RuneManager.Domain.Models;
    using RuneManager.Domain.Models.ValueObjects;

    public class AttackBruiserOptimizer : OptimizerBase
    {
        public override string Description
        {
            get
            {
                return
                    "A balanced build between damage over time and effective health for monsters whose damage scales with attack.";
            }
        }

        public override string Examples
        {
            get
            {
                return "Veromos, Tesarion, Fire MK, Wind MK, Anavel, Dragon Knights";
            }
        }

        protected override IList<Rune> FilterOutIrrelevantRunes(Monster monster, IList<Rune> runes, OptimizerSpeedType optimizerSpeed)
        {
            var slots = new[] { 1, 3, 5 };
            var mainStats = new List<StatType> { StatType.HPPercentage, StatType.DEFPercentage, StatType.ATKPercentage, StatType.CD, StatType.CR };
            var desiredStats = new List<StatType> { StatType.HPPercentage, StatType.DEFPercentage, StatType.ATKPercentage, StatType.CD, StatType.CR };

            var minStats = optimizerSpeed == OptimizerSpeedType.Fast ? 20 : 10;

            if (this.parameters.MinSpeed - monster.Speed >= 21)
            {
                mainStats.Add(StatType.SPD);
            }

            if (this.parameters.MinAccuracy - monster.Accuracy >= 32)
            {
                mainStats.Add(StatType.ACC);
            }

            if (this.parameters.MinResistance - monster.Resistance >= 32)
            {
                mainStats.Add(StatType.RES);
            }

            if (this.parameters.MinSpeed - monster.Speed > 0)
            {
                desiredStats.Add(StatType.SPD);
            }

            if (this.parameters.MinAccuracy - monster.Accuracy > 0)
            {
                desiredStats.Add(StatType.ACC);
            }

            if (this.parameters.MinResistance - monster.Resistance > 0)
            {
                desiredStats.Add(StatType.RES);
            }

            runes = runes.Where(p => p.TotalDesiredStats(desiredStats) > minStats).ToList();

            runes = this.FilterTop20PerSlot(runes, this.parameters, slots, mainStats, desiredStats);

            return runes;
        }

        protected override Func<RuneSet, double> OptimizedVariablePredicate()
        {
            return p => p.DOTMultiplier * p.EffectiveHealth;
        }

        public AttackBruiserOptimizer(IBuildingsRepository buildingsRepository)
            : base(buildingsRepository)
        {
        }
    }
}
