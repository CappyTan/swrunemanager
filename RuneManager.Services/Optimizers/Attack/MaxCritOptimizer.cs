﻿namespace RuneManager.Services.Optimizers.Attack
{
    using System;

    using RuneManager.Domain.Contracts.Repositories;
    using RuneManager.Domain.Models.ValueObjects;

    public class MaxCritOptimizer : DamageOptimizerBase
    {
        public override string Description
        {
            get
            {
                return
                    "Maximizes average damage per crit for monsters whose damage scales with attack. This is used for monsters who you expect to normally have 100% crit rate and you want to optimzie their burst damage.";
            }
        }

        public override string Examples
        {
            get
            {
                return "Lushen, Sieq, Theomars, Tyron, Sigmarus, Camilla, Kaz, Kahli";
            }
        }

        protected override Func<RuneSet, double> OptimizedVariablePredicate()
        {
            return p => p.CritMultiplier;
        }

        public MaxCritOptimizer(IBuildingsRepository buildingsRepository)
            : base(buildingsRepository)
        {
        }
    }
}
