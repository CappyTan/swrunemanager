﻿namespace RuneManager.Services.Optimizers.Attack
{
    using System;

    using RuneManager.Domain.Contracts.Repositories;
    using RuneManager.Domain.Models.ValueObjects;

    public class MaxHitOptimizer : DamageOptimizerBase
    {
        public override string Description
        {
            get
            {
                return
                    "Maximizes average damage per hit for monsters whose damage scales with attack. This is used for monsters who can not reach 100% crit rate and you want to optimzie them at a specific speed";
            }
        }

        public override string Examples
        {
            get
            {
                return "Ifrits, Rakshasa, Dragon Knights, Monkey Kings, Occult Girls, Tyron, Sigmarus, Perna";
            }
        }

        protected override Func<RuneSet, double> OptimizedVariablePredicate()
        {
            return p => p.HitMultiplier;
        }

        public MaxHitOptimizer(IBuildingsRepository buildingsRepository)
            : base(buildingsRepository)
        {
        }
    }
}
