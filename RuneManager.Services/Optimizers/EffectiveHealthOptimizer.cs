﻿namespace RuneManager.Services.Optimizers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using RuneManager.Domain.Base;
    using RuneManager.Domain.Contracts.Repositories;
    using RuneManager.Domain.Enumerations;
    using RuneManager.Domain.Models;
    using RuneManager.Domain.Models.ValueObjects;
    using RuneManager.Infrastructure;
    using RuneManager.Services.Calculators;

    public class EffectiveHealthOptimizer : OptimizerBase
    {
        public override string Description
        {
            get
            {
                return
                    "Maximizes effective health of a monster. This is good for all supports, healers, and  tanks that are not designed to do damage or be as fast as possible.";
            }
        }

        public override string Examples
        {
            get
            {
                return "Belladeon, Chasun, Verdehile, Veromos, Tesarion, Shannon, Death Knights, Brownie Magicians, Baretta, Tyron, Kona, Ahman";
            }
        }

        protected override IList<Rune> FilterOutIrrelevantRunes(Monster monster, IList<Rune> runes, OptimizerSpeedType optimizerSpeed)
        {
            var slots = new[] { 1, 3, 5 };
            var mainStats = new List<StatType> { StatType.HPPercentage, StatType.DEFPercentage };
            var desiredStats = new List<StatType> { StatType.HPPercentage, StatType.DEFPercentage };

            var minStats = optimizerSpeed == OptimizerSpeedType.Fast ? ConfigSettings.Fast : ConfigSettings.Smart;

            if (parameters.MinSpeed - monster.Speed >= 21)
            {
                mainStats.Add(StatType.SPD);
            }

            if (parameters.MinAccuracy - monster.Accuracy >= 32)
            {
                mainStats.Add(StatType.ACC);
            }

            if (parameters.MinResistance - monster.Resistance >= 32)
            {
                mainStats.Add(StatType.RES);
            }

            if (parameters.MinCritChance - monster.CritChance >= 29)
            {
                mainStats.Add(StatType.CR);
            }

            if (parameters.MinSpeed - monster.Speed > 0)
            {
                desiredStats.Add(StatType.SPD);
            }

            if (parameters.MinAccuracy - monster.Accuracy > 0)
            {
                desiredStats.Add(StatType.ACC);
            }

            if (parameters.MinResistance - monster.Resistance > 0)
            {
                desiredStats.Add(StatType.RES);
            }

            if (parameters.MinCritChance - monster.CritChance > 0)
            {
                desiredStats.Add(StatType.CR);
            }

            runes = runes.Where(p => p.TotalDesiredStats(desiredStats) > minStats).ToList();

            runes = this.FilterTop20PerSlot(runes, parameters, slots, mainStats, desiredStats);

            return runes;
        }

        protected override Func<RuneSet, double> OptimizedVariablePredicate()
        {
            return p => p.EffectiveHealth;
        }

        public EffectiveHealthOptimizer(IBuildingsRepository buildingsRepository)
            : base(buildingsRepository)
        {
        }
    }
}
