﻿namespace RuneManager.Services.Optimizers
{
    using System;

    using RuneManager.Domain.Contracts.Repositories;
    using RuneManager.Domain.Models.ValueObjects;
    using RuneManager.Services.Optimizers.Attack;

    public class EnemyMaxHPOptimizer : DamageOptimizerBase
    {
        public override string Description
        {
            get
            {
                return
                    "Maximizes average damage per hit for monsters whose damage scales with enemy max HP. This is used for nukers whose primary damage is coming from Enemy Max HP attacks.";
            }
        }

        public override string Examples
        {
            get
            {
                return "Sigmarus";
            }
        }

        protected override Func<RuneSet, double> OptimizedVariablePredicate()
        {
            return p => p.CritCoefficient;
        }

        public EnemyMaxHPOptimizer(IBuildingsRepository buildingsRepository)
            : base(buildingsRepository)
        {
        }
    }
}
