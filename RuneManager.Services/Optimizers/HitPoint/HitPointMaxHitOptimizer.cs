﻿namespace RuneManager.Services.Optimizers.HitPoint
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using RuneManager.Domain.Base;
    using RuneManager.Domain.Contracts.Repositories;
    using RuneManager.Domain.Enumerations;
    using RuneManager.Domain.Models;
    using RuneManager.Domain.Models.ValueObjects;
    using RuneManager.Infrastructure;

    public class HitPointMaxHitOptimizer : OptimizerBase
    {
        protected override IList<Rune> FilterOutIrrelevantRunes(Monster monster, IList<Rune> runes, OptimizerSpeedType optimizerSpeed)
        {
            var slots = new[] { 1, 3, 5 };
            var mainStats = new List<StatType> { StatType.HPPercentage, StatType.CR, StatType.CD, };
            var desiredStats = new List<StatType> { StatType.HPPercentage, StatType.ATKPercentage, StatType.CR, StatType.CD };

            var minStats = optimizerSpeed == OptimizerSpeedType.Fast ? ConfigSettings.Fast : ConfigSettings.Smart;
            

            if (this.parameters.MinSpeed - monster.Speed >= 21)
            {
                mainStats.Add(StatType.SPD);
            }

            if (this.parameters.MinSpeed - monster.Speed > 0)
            {
                desiredStats.Add(StatType.SPD);
            }

            if (this.parameters.MinAccuracy - monster.Accuracy >= 32)
            {
                mainStats.Add(StatType.ACC);
                desiredStats.Add(StatType.ACC);
            }

            if (this.parameters.MinResistance - monster.Resistance >= 32)
            {
                mainStats.Add(StatType.RES);
                desiredStats.Add(StatType.RES);
            }

            runes = runes.Where(p => p.TotalDesiredStats(desiredStats) > minStats).ToList();

            runes = this.FilterTop20PerSlot(runes, this.parameters, slots, mainStats, desiredStats);

            return runes;
        }

        public override string Description
        {
            get
            {
                return
                    "Maximizes average damage per hit for monsters whose damage scales with hit points. This is used for monsters who can not reach 100% crit rate and you want to optimzie them at a specific speed";
            }
        }

        public override string Examples
        {
            get
            {
                return "Rakan, Death Knights, Vagabonds, Werewolves";
            }
        }

        protected override Func<RuneSet, double> OptimizedVariablePredicate()
        {
            return p => p.HitMultiplier;
        }

        public HitPointMaxHitOptimizer(IBuildingsRepository buildingsRepository)
            : base(buildingsRepository)
        {
        }
    }
}
