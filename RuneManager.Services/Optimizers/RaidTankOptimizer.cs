﻿namespace RuneManager.Services.Optimizers
{
    using System;

    using RuneManager.Domain.Contracts.Repositories;
    using RuneManager.Domain.Models.ValueObjects;

    public class RaidTankOptimizer : EffectiveHealthOptimizer
    {
        public RaidTankOptimizer(IBuildingsRepository buildingsRepository)
            : base(buildingsRepository)
        {
        }

        protected override Func<RuneSet, double> OptimizedVariablePredicate()
        {
            return p => p.EffectiveRaidFrontLineHealth;
        }
    }
}
