﻿namespace RuneManager.Services
{
    using RuneManager.Domain;
    using RuneManager.Domain.Contracts.Repositories;
    using RuneManager.Domain.Contracts.Services;
    using RuneManager.Domain.Models;

    public class RuneService : IRuneService
    {
        private readonly IRuneRepository runeRepository;

        public RuneService(IRuneRepository runeRepository)
        {
            this.runeRepository = runeRepository;
        }

        public void AddRune(Rune rune)
        {
            this.runeRepository.Add(rune);
        }

        public Rune GetRune(int id)
        {
            return this.runeRepository.Find(id);
        }

        public void UpdateRune(Rune rune)
        {
            this.runeRepository.Update(rune);
            rune.ClearStatCache();
        }

        public void SellRune(Rune rune)
        {
            this.runeRepository.Delete(rune);
        }
    }
}
