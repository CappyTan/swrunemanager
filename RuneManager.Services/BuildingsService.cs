﻿namespace RuneManager.Services
{
    using RuneManager.Domain.Contracts.Repositories;
    using RuneManager.Domain.Contracts.Services;
    using RuneManager.Domain.Models;

    public class BuildingsService : IBuildingsService
    {
        private readonly IBuildingsRepository buildingsRepository;

        public BuildingsService(IBuildingsRepository buildingsRepository)
        {
            this.buildingsRepository = buildingsRepository;
        }

        public void Update(Buildings buildings)
        {
            this.buildingsRepository.Update(buildings);
        }
    }
}
