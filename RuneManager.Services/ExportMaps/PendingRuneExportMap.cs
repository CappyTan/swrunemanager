﻿namespace RuneManager.Services.ExportMaps
{
    using System.Collections.Generic;
    using System.Linq;

    using CsvHelper.Configuration;
    using CsvHelper.TypeConversion;

    using RuneManager.Domain.Models;

    public class PendingRuneExportMap : CsvClassMap<Monster>
    {
        public PendingRuneExportMap()
        {
            Map(p => p.Name);
            Map(p => p.PendingRunes).Name("Slot 1").TypeConverter<Slot1RuneConverter>();
            Map(p => p.PendingRunes).Name("Slot 2").TypeConverter<Slot2RuneConverter>();
            Map(p => p.PendingRunes).Name("Slot 3").TypeConverter<Slot3RuneConverter>();
            Map(p => p.PendingRunes).Name("Slot 4").TypeConverter<Slot4RuneConverter>();
            Map(p => p.PendingRunes).Name("Slot 5").TypeConverter<Slot5RuneConverter>();
            Map(p => p.PendingRunes).Name("Slot 6").TypeConverter<Slot6RuneConverter>();

        }
    }

    public class RuneConverter : DefaultTypeConverter
    {
        protected int Slot;

        public override string ConvertToString(TypeConverterOptions options, object value)
        {
            var returnString = string.Empty;

            if (value == null)
            {
                return returnString;
            }

            var runes = value as IEnumerable<Rune>;
            var rune = runes.First(p => p.Slot == Slot);
            
            
            returnString += string.Join(",", rune.Stats.Select(p => p.ReadableValue)) + "," + (rune.Monster != null ? rune.Monster.Name : string.Empty);

            return returnString.Trim();
        }
    }

    public class Slot1RuneConverter : RuneConverter
    {
        public override string ConvertToString(TypeConverterOptions options, object value)
        {
            this.Slot = 1;
            return base.ConvertToString(options, value);
        }
    }

    public class Slot2RuneConverter : RuneConverter
    {
        public override string ConvertToString(TypeConverterOptions options, object value)
        {
            this.Slot = 2;
            return base.ConvertToString(options, value);
        }
    }

    public class Slot3RuneConverter : RuneConverter
    {
        public override string ConvertToString(TypeConverterOptions options, object value)
        {
            this.Slot = 3;
            return base.ConvertToString(options, value);
        }
    }

    public class Slot4RuneConverter : RuneConverter
    {
        public override string ConvertToString(TypeConverterOptions options, object value)
        {
            this.Slot = 4;
            return base.ConvertToString(options, value);
        }
    }

    public class Slot5RuneConverter : RuneConverter
    {
        public override string ConvertToString(TypeConverterOptions options, object value)
        {
            this.Slot = 5;
            return base.ConvertToString(options, value);
        }
    }

    public class Slot6RuneConverter : RuneConverter
    {
        public override string ConvertToString(TypeConverterOptions options, object value)
        {
            this.Slot = 6;
            return base.ConvertToString(options, value);
        }
    }
}
