﻿namespace RuneManager.Services.Calculators
{
    using RuneManager.Domain.Enumerations;
    using RuneManager.Domain.Models;

    public class GlassCannonCalculator : CalculatorBase
    {
        protected override decimal GetStatValue(Stat stat)
        {
            if (!stat.Value.HasValue)
            {
                return 0;
            }

            if (stat.Type == StatType.ATKPercentage || stat.Type == StatType.CR || stat.Type == StatType.CD)
            {
                return stat.Value.Value;
            }

            return 0;
        }
    }
}
