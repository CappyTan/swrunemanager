﻿namespace RuneManager.Services.Calculators
{
    using System.ComponentModel;

    using RuneManager.Domain.Enumerations;
    using RuneManager.Domain.Models;

    public class TotalStatsCalculator : CalculatorBase
    {
        private readonly decimal hpWeight;

        private readonly decimal defWeight;

        private readonly decimal atkWeight;

        private readonly decimal resWeight;

        private readonly decimal accWeight;

        private readonly decimal spdWeight;

        private readonly decimal crWeight;

        private readonly decimal cdWeight;


        public TotalStatsCalculator()
            : this(new decimal?(), new decimal?(), new decimal?(), new decimal?(), new decimal?(), new decimal?(), new decimal?(), new decimal?())
        {
            
        }

        public TotalStatsCalculator(decimal? HPWeight, decimal? DEFWeight, decimal? ATKWeight, decimal? RESWeight, decimal? ACCWeight, decimal? SPDWeight, decimal? CRWeight, decimal? CDWeight)
        {
            this.hpWeight = HPWeight ?? 1.0m;
            this.defWeight = DEFWeight ?? 1.0m;
            this.atkWeight = ATKWeight ?? 1.0m;
            this.resWeight = RESWeight ?? 1.0m;
            this.accWeight = ACCWeight ?? 1.0m;
            this.spdWeight = SPDWeight ?? (8.0m / 6.0m);
            this.crWeight = CRWeight ?? (8.0m / 7.0m);
            this.cdWeight = CDWeight ?? (8.0m / 7.0m);
            
        }

        protected override decimal GetStatValue(Stat stat)
        {
            if (!stat.Value.HasValue)
            {
                return 0;
            }

            if (stat.Type == StatType.ACC)
            {
                return stat.Value.Value * accWeight;
            }

            if (stat.Type == StatType.ATKPercentage)
            {
                return stat.Value.Value * atkWeight;
            }

            if (stat.Type == StatType.DEFPercentage)
            {
                return stat.Value.Value * defWeight;
            }

            if (stat.Type == StatType.RES)
            {
                return stat.Value.Value * resWeight;
            }

            if (stat.Type == StatType.HPPercentage)
            {
                return stat.Value.Value * hpWeight;
            }

            if (stat.Type == StatType.ATK)
            {
                return (stat.Value.Value * atkWeight) / 8.0000m;
            }

            if (stat.Type == StatType.HP)
            {
                return (stat.Value.Value * hpWeight) / 100.0000m;
            }

            if (stat.Type == StatType.DEF)
            {
                return (stat.Value.Value * defWeight) / 5.5000m;
            }

            if (stat.Type == StatType.CD)
            {
                return stat.Value.Value * cdWeight;
            }

            if (stat.Type == StatType.CR)
            {
                return stat.Value.Value * crWeight;
            }

            if (stat.Type == StatType.SPD)
            {
                return stat.Value.Value * spdWeight;
            }


            throw new InvalidEnumArgumentException("Type Not Found");
        }
    }
}

