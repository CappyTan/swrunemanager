namespace RuneManager.Services.Calculators
{
    using System.Collections.Generic;

    using RuneManager.Domain.Models;

    public abstract class CalculatorBase
    {
        public decimal Calculate(Rune rune)
        {
            decimal value = 0;

            value += this.GetStatValue(rune.MainStat);
            value += this.GetStatValue(rune.BonusStat);
            value += this.GetStatValue(rune.Stat1);
            value += this.GetStatValue(rune.Stat2);
            value += this.GetStatValue(rune.Stat3);
            value += this.GetStatValue(rune.Stat4);

            return value;
        }

        public decimal Calculate(IList<Rune> runes)
        {
            decimal value = 0;
            foreach (var rune in runes)
            {
                value += this.Calculate(rune);
            }

            return value;
        }

        protected abstract decimal GetStatValue(Stat stat);
    }
}