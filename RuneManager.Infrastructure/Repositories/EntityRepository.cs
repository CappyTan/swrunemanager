﻿namespace RuneManager.Infrastructure.Repositories
{
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq;

    using RuneManager.Domain.Contracts.Repositories;
    using RuneManager.Infrastructure.Database;

    public abstract class EntityRepository<T> : IRepository<T> where T : class
    {
        protected readonly RuneManagerContext context;

        public EntityRepository(RuneManagerContext context)
        {
            this.context = context;
        }

        public virtual void Add(T entity)
        {
            context.Set<T>().Add(entity);
            context.SaveChanges();
        }

        public void Update(T entity)
        {
            context.Entry(entity).State = EntityState.Modified;
            context.SaveChanges();
        }

        public void Delete(T entity)
        {
            context.Set<T>().Remove(entity);
            context.SaveChanges();   
        }

        public T Find(int id)
        {
            return context.Set<T>().Find(id);
        }

        public IList<T> GetAll()
        {
            return context.Set<T>().ToList();
        }
    }
}
