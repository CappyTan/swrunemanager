﻿namespace RuneManager.Infrastructure.Repositories
{
    using System.Linq;

    using RuneManager.Domain.Contracts.Repositories;
    using RuneManager.Domain.Models;
    using RuneManager.Infrastructure.Database;

    public class BuildingsRepository : EntityRepository<Buildings>, IBuildingsRepository
    {
        public BuildingsRepository(RuneManagerContext context)
            : base(context)
        {
        }

        public Buildings GetBuildings()
        {
            var buildings = this.GetAll();

            if (!buildings.Any())
            {
                var newBuildings = new Buildings();
                this.Add(newBuildings);

                return newBuildings;
            }

            return buildings.First();
        }
    }
}
