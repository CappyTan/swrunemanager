﻿namespace RuneManager.Infrastructure.Repositories
{
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq;

    using RuneManager.Domain.Contracts.Repositories;
    using RuneManager.Domain.Models;
    using RuneManager.Infrastructure.Database;

    public class MonsterEntityRepository : EntityRepository<Monster>, IMonsterRepository
    {
        public MonsterEntityRepository(RuneManagerContext context)
            : base(context)
        {
            
        }

        public IList<Monster> GetMonsters()
        {
            return context.Monsters.ToList();
            
        }
    }
}
