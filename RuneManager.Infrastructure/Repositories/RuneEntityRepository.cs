﻿namespace RuneManager.Infrastructure.Repositories
{
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;

    using RuneManager.Domain;
    using RuneManager.Domain.Contracts.Repositories;
    using RuneManager.Domain.Models;
    using RuneManager.Infrastructure.Database;

    public class RuneEntityRepository : EntityRepository<Rune>, IRuneRepository
    {
        public RuneEntityRepository(RuneManagerContext context)
            : base(context)
        {
        }

       public IList<Rune> GetRunes()
        {
            return context.Runes.ToList();
        }
    }
}
