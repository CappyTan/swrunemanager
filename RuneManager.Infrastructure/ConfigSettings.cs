﻿namespace RuneManager.Infrastructure
{
    using System;
    using System.Configuration;

    public static class ConfigSettings
    {
        public static int Smart
        {
            get
            {
                return Convert.ToInt32(ConfigurationManager.AppSettings["Smart"]);
            }
            
        }

        public static int Fast
        {
            get
            {
                return Convert.ToInt32(ConfigurationManager.AppSettings["Fast"]);
            }

        }
    }
}
