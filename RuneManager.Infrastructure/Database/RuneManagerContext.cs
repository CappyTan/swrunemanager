﻿namespace RuneManager.Infrastructure.Database
{
    using System.Data.Entity;

    using RuneManager.Domain;
    using RuneManager.Domain.Models;
    using RuneManager.Infrastructure.Database.Mappings;

    public class RuneManagerContext : DbContext
    {
        public DbSet<Rune> Runes { get; set; }

        public DbSet<Monster> Monsters { get; set; }

        public DbSet<Buildings> Buildings { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Properties().Where(p => p.Name == "Id").Configure(p => p.IsKey());

            modelBuilder.Configurations.Add(new RuneMap());
            modelBuilder.Configurations.Add(new MonsterMap());
        }
    }

    
}
