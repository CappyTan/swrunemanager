﻿namespace RuneManager.Infrastructure.Database.Mappings
{
    using System.Data.Entity.ModelConfiguration;

    using RuneManager.Domain;
    using RuneManager.Domain.Models;

    public class RuneMap : EntityTypeConfiguration<Rune>
    {
        public RuneMap()
        {
            ToTable("Runes");

            Property(p => p.Type).HasColumnName("Set");
            Property(p => p.MonsterId).HasColumnName("MonsterId");
            Property(p => p.PendingMonsterId).HasColumnName("PendingMonsterId");
            Property(p => p.Slot).HasColumnName("Slot");
            Property(p => p.MainStat.Type).HasColumnName("MainStatType");
            Property(p => p.MainStat.Value).HasColumnName("MainStatValue");
            Property(p => p.BonusStat.Type).IsOptional().HasColumnName("BonusStatType");
            Property(p => p.BonusStat.Value).IsOptional().HasColumnName("BonusStatValue");
            Property(p => p.Stat1.Type).HasColumnName("Stat1Type");
            Property(p => p.Stat1.Value).HasColumnName("Stat1Value");
            Property(p => p.Stat2.Type).HasColumnName("Stat2Type");
            Property(p => p.Stat2.Value).HasColumnName("Stat2Value");
            Property(p => p.Stat3.Type).HasColumnName("Stat3Type");
            Property(p => p.Stat3.Value).HasColumnName("Stat3Value");
            Property(p => p.Stat4.Type).HasColumnName("Stat4Type");
            Property(p => p.Stat4.Value).HasColumnName("Stat4Value");
            Property(p => p.Locked).HasColumnName("Locked");

            HasOptional(p => p.Monster).WithMany(r => r.Runes);
            HasOptional(p => p.PendingMonster).WithMany(r => r.PendingRunes);
        }
    }
}
