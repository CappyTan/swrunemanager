﻿namespace RuneManager.Infrastructure.Database.Mappings
{
    using System.Data.Entity.ModelConfiguration;

    using RuneManager.Domain.Models;

    public class MonsterMap : EntityTypeConfiguration<Monster>
    {
        public MonsterMap()
        {
            ToTable("Monsters");

            Property(p => p.Accuracy).HasColumnName("Accuracy");
            Property(p => p.Attack).HasColumnName("Attack");
            Property(p => p.CritChance).HasColumnName("CritChance");
            Property(p => p.CritDamage).HasColumnName("CritDamage");
            Property(p => p.Defense).HasColumnName("Defense");
            Property(p => p.Element).HasColumnName("Element");
            Property(p => p.HitPoints).HasColumnName("HitPoints");
            Property(p => p.Name).HasColumnName("Name");
            Property(p => p.Resistance).HasColumnName("Resistance");
            Property(p => p.Speed).HasColumnName("Speed");

            HasMany(p => p.Runes).WithOptional(p => p.Monster).HasForeignKey(p => p.MonsterId);
            HasMany(p => p.PendingRunes).WithOptional(p => p.PendingMonster).HasForeignKey(p => p.PendingMonsterId);
        }
    }
}
