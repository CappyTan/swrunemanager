﻿namespace RuneManager.Domain.Contracts.Repositories
{
    using RuneManager.Domain.Models;

    public interface IBuildingsRepository : IRepository<Buildings>
    {
        Buildings GetBuildings();
    }
}
