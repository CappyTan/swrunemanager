﻿namespace RuneManager.Domain.Contracts.Repositories
{
    using System.Collections.Generic;

    public interface IRepository<T>
    {
        void Add(T entity);

        void Update(T entity);

        void Delete(T entity);

        T Find(int id);

        IList<T> GetAll();
    }
}
