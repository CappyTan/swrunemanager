﻿namespace RuneManager.Domain.Contracts.Repositories
{
    using System.Collections.Generic;

    using RuneManager.Domain.Models;

    public interface IMonsterRepository : IRepository<Monster>
    {
        IList<Monster> GetMonsters();
    }
}
