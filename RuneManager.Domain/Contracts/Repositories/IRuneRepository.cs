﻿namespace RuneManager.Domain.Contracts.Repositories
{
    using System.Collections.Generic;

    using RuneManager.Domain.Models;

    public interface IRuneRepository : IRepository<Rune>
    {
        IList<Rune> GetRunes();
    }
}
