﻿namespace RuneManager.Domain.Contracts.Services
{
    using RuneManager.Domain.Models;

    public interface IMonsterService
    {
        void AddMonster(Monster monster);

        string ExportPendingRunes();

        void UpdateMonster(Monster monster);
    }
}
