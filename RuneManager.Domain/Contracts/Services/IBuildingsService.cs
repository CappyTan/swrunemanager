﻿namespace RuneManager.Domain.Contracts.Services
{
    using RuneManager.Domain.Models;

    public interface IBuildingsService
    {
        void Update(Buildings buildings);
    }
}
