﻿namespace RuneManager.Domain.Contracts.Services
{
    using RuneManager.Domain.Models;

    public interface IRuneService
    {
        void AddRune(Rune rune);

        Rune GetRune(int id);

        void UpdateRune(Rune rune);

        void SellRune(Rune rune);
    }
}
