﻿namespace RuneManager.Domain.Helpers
{
    using System.Collections.Generic;

    using RuneManager.Domain.Enumerations;

    public static class MaxStats
    {
        public static int GetValue(StatType statType, int runeLevel)
        {
            return values[statType][runeLevel];
        }

        private static readonly Dictionary<StatType, int[]> values = new Dictionary<StatType, int[]>
                                                                {
                                                                    { StatType.HP, new int[] { 0,804,1092,1380,1704,2088,2448 } },
                                                                    { StatType.ATK, new int[] { 0,54,73,92,112,135,160 } },
                                                                    { StatType.DEF, new int[] { 0,54,73,92,112,135,160 } },
                                                                    { StatType.HPPercentage, new int[] { 0,18,19,38,43,51,63 } },
                                                                    { StatType.ATKPercentage, new int[] { 0,18,19,38,43,51,63 } },
                                                                    { StatType.DEFPercentage, new int[] { 0,18,19,38,43,51,63 } },
                                                                    { StatType.SPD, new int[] { 0,18,19,25,30,39,42 } },
                                                                    { StatType.CR, new int[] { 0,18,19,37,41,47,58 } },
                                                                    { StatType.CD, new int[] { 0,19,37,43,57,65,80 } },
                                                                    { StatType.ACC, new int[] { 0,18,19,38,44,51,64 } },
                                                                    { StatType.RES, new int[] { 0,18,19,38,44,51,64 } }

                                                                };

       
    }
}
