﻿namespace RuneManager.Domain.Helpers
{
    using System.Collections.Generic;
    using System.Linq;

    using RuneManager.Domain.Enumerations;
    using RuneManager.Domain.Models;

    public static class SetBonusHelper
    {
        public static int GetFocusBonus(IEnumerable<Rune> runeSet)
        {
            return GetTwoSetBonus(runeSet, SetType.Focus, 20);
        }

        public static int GetEndureBonus(IEnumerable<Rune> runeSet)
        {
            return GetTwoSetBonus(runeSet, SetType.Endure, 20);
        }

        public static int GetEnergyBonus(IEnumerable<Rune> runeSet)
        {
            return GetTwoSetBonus(runeSet, SetType.Energy, 15);
        }

        public static int GetGuardBonus(IEnumerable<Rune> runeSet)
        {
            return GetTwoSetBonus(runeSet, SetType.Guard, 15);
        }

        public static double GetSwiftBonus(Monster monster, IEnumerable<Rune> runeSet)
        {
            if (runeSet.Count(p => p.Type == SetType.Swift) >= 4)
            {
                return monster.Speed * 0.25;
            }

            return 0;
        }

        public static int GetBladeBonus(IEnumerable<Rune> runeSet)
        {
            return GetTwoSetBonus(runeSet, SetType.Blade, 12);
        }

        private static int GetTwoSetBonus(IEnumerable<Rune> runeSet, SetType set, int bonusAmount)
        {
            var runesOfSet = runeSet.Count(p => p.Type == set);

            if (runesOfSet <= 1)
            {
                return 0;
            }

            if (runesOfSet <= 3)
            {
                return bonusAmount;
            }

            if (runesOfSet <= 5)
            {
                return bonusAmount * 2;
            }

            return bonusAmount * 3;
        }
    }
}
