﻿namespace RuneManager.Domain.Models
{
    using RuneManager.Domain.Enumerations;

    using WellPoint.Marketing.CampaignTracker.Framework.Extensions;

    public class Stat
    {
        public StatType? Type { get; set; }

        public int? Value { get; set; }

        public string ReadableValue
        {
            get
            {
                if (!Type.HasValue)
                {
                    return string.Empty;
                }

                return string.Format("{0} {1}", Value, Type.Value.GetDescription());
            }
        }
    }
}
