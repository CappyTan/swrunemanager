﻿namespace RuneManager.Domain.Models
{
    using RuneManager.Domain.Enumerations;

    public class Set
    {
        public SetType Type { get; set; }

        public int Pieces { get; set; }
    }
}
