﻿namespace RuneManager.Domain.Models
{
    using RuneManager.Domain.Base;

    public class Buildings : Entity
    {
        public int DefenseTower { get; set; }
        public int AttackTower { get; set; }
        public int CritDamageTower { get; set; }
        public int HitPointTower { get; set; }
        public int WindTower { get; set; }
        public int FireTower { get; set; }
        public int WaterTower { get; set; }
        public int DarkTower { get; set; }
        public int LightTower { get; set; }

        public int SpeedTower { get; set; }

        public int DefenseFlag { get; set; }
        public int AttackFlag { get; set; }
        public int CritDamageFlag { get; set; }
        public int HitPointFlag { get; set; }
    }
}
