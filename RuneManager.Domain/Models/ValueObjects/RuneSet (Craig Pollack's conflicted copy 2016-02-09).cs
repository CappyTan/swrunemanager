namespace RuneManager.Domain.Models.ValueObjects
{
    using System.Collections.Generic;

    using RuneManager.Domain.Models;

    public class RuneSet
    {
        public IList<Rune> Runes { get; set; }

        public double TotalCrit { get; set; }

        public double CritMultiplier { get; set; }

        public double HitMultiplier { get; set; }

        public double TotalSpeed { get; set; }

        public int TotalAccuracy { get; set; }

        public double TotalHitPoints { get; set; }

        public double EffectiveHealth { get; set; }

        public int TotalResistance { get; set; }

        public double DOTMultiplier { get; set; }

        public double CritCoefficient { get; set; }

        public double AttackCoefficient { get; set; }
    }
}