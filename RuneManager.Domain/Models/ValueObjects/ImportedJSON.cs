﻿namespace RuneManager.Domain.Models.ValueObjects
{
    using System.Collections.Generic;

    public class ImportedJSON
    {
        public ImportedJSON()
        {
            this.mons = new List<ImportedRune>();
            this.runes = new List<ImportedRune>();
        }

        public IList<ImportedRune> mons { get; set; }

        public IList<ImportedRune> runes { get; set; }
    }
}
