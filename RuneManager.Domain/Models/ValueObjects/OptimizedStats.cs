﻿namespace RuneManager.Domain.Models.ValueObjects
{
    public class OptimizedStats
    {
        public double HitPoints { get; set; }

        public double TotalDefense { get; set; }

        public double EffectiveHitPoints { get; set; }

        public double Speed { get; set; }

        public int CritChance { get; set; }

        public int CritDamage { get; set; }

        public int Resistance { get; set; }

        public int Accuracy { get; set; }

        public double TotalAttack { get; set; }

        public int TotalCritDamage { get; set; }

        public double Defense { get; set; }
    }
}
