﻿namespace RuneManager.Domain.Models.ValueObjects
{
    using System;

    using RuneManager.Domain.Enumerations;

    public class OptimizerParameters
    {
        public OptimizerParameters()
        {
            MaxSpeed = Int32.MaxValue;
            MaxCritChance = Int32.MaxValue;
        }

        public int MinHitPoints { get; set; }
        public int MinEffectiveHitPoints { get; set; }
        public int MinSpeed { get; set; }
        public int? MaxSpeed { get; set; }
        public int MinCritChance { get; set; }
        public int MaxCritChance { get; set; }
        public int MinAccuracy { get; set; }
        public int MinResistance { get; set; }
        public bool CritBuff { get; set; }
        public StatType? LeaderSkillType { get; set; }
        public int? LeaderSkillValue { get; set; }
        public SetType? FourSet { get; set; }
        public SetType? TwoSet { get; set; }
        public SetType? TwoSet2 { get; set; }
        public SetType? TwoSet3 { get; set; }
        public BalanceType Balance { get; set; }

        public ScalingType ScalingType { get; set; }

        public int SkillUp { get; set; }

        public bool IsGuildWars { get; set; }

        public OptimizerSpeedType OptimizerSpeed { get; set; }

        public bool IgnoreEquipped { get; set; }
        public int MinDefense { get; set; }
    }
}
