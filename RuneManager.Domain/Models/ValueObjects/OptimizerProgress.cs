﻿namespace RuneManager.Domain.Models.ValueObjects
{
    using System;

    public class OptimizerProgress
    {
        public long TotalCombinations { get; set; }

        public long CurrentCombination { get; set; }
    }
}
