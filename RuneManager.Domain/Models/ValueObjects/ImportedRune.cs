﻿namespace RuneManager.Domain.Models.ValueObjects
{
    using RuneManager.Domain.Enumerations;
    using RuneManager.Domain.Helpers;

    using WellPoint.Marketing.CampaignTracker.Framework.Extensions;

    // ReSharper disable InconsistentNaming
    public class ImportedRune
    {
        //public string sub_hpp { get; set; }
        public string set { get; set; }
		public int grade { get; set; }
		//public string sub_hpf { get; set; }
		//public string sub_acc { get; set; }
		public int i_v { get; set; }
		public string i_t { get; set; }
		//"id": 205,
		public int slot { get; set; }
        public string s1_t { get; set; }
        public int s1_v { get; set; }
        public string s2_t { get; set; }
        public int s2_v { get; set; }
        public string s3_t { get; set; }
        public int s3_v { get; set; }
        public string s4_t { get; set; }
        public int s4_v { get; set; }
		
        public int m_v { get; set; }
        public string m_t { get; set; }

        public string monster_n { get; set; }
        
		//"sub_crate": 4,
		//"sub_spd": "-",
		
		
		
		//"sub_atkf": 28,
		//"sub_res": 16,
		//"locked": 0,
		//"level": 12,
		//"sub_deff": "-",
		
		//"sub_cdmg": "-",
		//"sub_atkp": "-",
		
		//"sub_defp": "-",
		//"m_t": "HP flat"

        public Rune ToRune(bool useMaxLevelMainStat, bool includeType)
        {
            var rune = new Rune();
            if (includeType)
            {
                try
                {
                    rune.Type = EnumerationExtensions.ConvertStringToEnum<SetType>(this.set);
                }
                catch
                {
                    rune.Type = SetType.Fight;
                }
                
            }
            rune.Slot = this.slot;
            rune.MainStat = this.GetStat(this.m_t, this.m_v, useMaxLevelMainStat);
            rune.BonusStat = this.GetStat(this.i_t, this.i_v, false);
            rune.Stat1 = this.GetStat(this.s1_t, this.s1_v, false);
            rune.Stat2 = this.GetStat(this.s2_t, this.s2_v, false);
            rune.Stat3 = this.GetStat(this.s3_t, this.s3_v, false);
            rune.Stat4 = this.GetStat(this.s4_t, this.s4_v, false);

            return rune;
        }

        private Stat GetStat(string type, int value, bool useMaxLevelMainStat)
        {
            var stat = new Stat();

            switch (type)
            {
                case "HP flat":
                    stat.Type = StatType.HP;
                    break;
                case "DEF flat":
                    stat.Type = StatType.DEF;
                    break;
                case "ATK flat":
                    stat.Type = StatType.ATK;
                    break;
                case "HP%":
                    stat.Type = StatType.HPPercentage;
                    break;
                case "DEF%":
                    stat.Type = StatType.DEFPercentage;
                    break;
                case "ATK%":
                    stat.Type = StatType.ATKPercentage;
                    break;
                case "ACC":
                    stat.Type = StatType.ACC;
                    break;
                case "RES":
                    stat.Type = StatType.RES;
                    break;
                case "SPD":
                    stat.Type = StatType.SPD;
                    break;
                case "CDmg":
                    stat.Type = StatType.CD;
                    break;
                case "CRate":
                    stat.Type = StatType.CR;
                    break;
                default:
                    return stat;
            }

            if (useMaxLevelMainStat)
            {
                stat.Value = MaxStats.GetValue(stat.Type.Value, this.grade);
            }
            else
            {
                stat.Value = value;
            }
            

            return stat;
        }
    }
    // ReSharper restore InconsistentNaming
}
