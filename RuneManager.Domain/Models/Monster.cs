﻿namespace RuneManager.Domain.Models
{
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;

    using RuneManager.Domain.Base;
    using RuneManager.Domain.Enumerations;
    using RuneManager.Domain.Helpers;

    public class Monster : Entity
    {
        public Monster()
        {
            this.Runes = new List<Rune>();
            this.PendingRunes = new List<Rune>();
        }

        public string Name { get; set; }

        public Element Element { get; set; }

        public int HitPoints { get; set; }

        public int Defense { get; set; }

        public int Attack { get; set; }

        public int Speed { get; set; }

        public int CritChance { get; set; }

        public int CritDamage { get; set; }

        public int Resistance { get; set; }

        public int Accuracy { get; set; }

        public virtual ICollection<Rune> Runes { get; set; }

        public virtual ICollection<Rune> PendingRunes { get; set; }

        public double TotalAttack(IEnumerable<Rune> runes)
        {
            
            var totalAtk = runes.SelectMany(p => p.Stats.Where(st => st.Type.Value == StatType.ATK)).Sum(p => p.Value) ?? 0;
            var totalAtkPercentage = (runes.SelectMany(p => p.Stats.Where(st => st.Type.Value == StatType.ATKPercentage)).Sum(p => p.Value) ?? 0.0) / 100;

            if (runes.Count(p => p.Type == SetType.Fatal) >= 4)
            {
                totalAtkPercentage += .35;
            }

            return (this.Attack + 0.0) * (1 + totalAtkPercentage) + totalAtk;
            
        }

        public int TotalCritDamage(IEnumerable<Rune> runes)
        {           
            var cd = runes.SelectMany(p => p.Stats.Where(st => st.Type.Value == StatType.CD)).Sum(p => p.Value) ?? 0;

            if (runes.Count(p => p.Type == SetType.Rage) >= 4)
            {
                cd += 40;
            }

            return this.CritDamage + cd;
        }

        public double TotalHitPoints(IEnumerable<Rune> runes)
        {
            var totalBonusHP = runes.SelectMany(p => p.Stats.Where(st => st.Type == StatType.HP)).Sum(p => p.Value) ?? 0.0;
            var totalBonusPercentage = runes.SelectMany(p => p.Stats.Where(st => st.Type == StatType.HPPercentage)).Sum(p => p.Value) + SetBonusHelper.GetEnergyBonus(runes) ?? 0.0;

            totalBonusPercentage /= 100;

            return totalBonusHP + this.HitPoints * (1 + totalBonusPercentage);
        }

        public double TotalDefense(IEnumerable<Rune> runes)
        {
            var totalBonusDEF = runes.SelectMany(p => p.Stats.Where(st => st.Type == StatType.DEF)).Sum(p => p.Value) ?? 0.0;
            var totalBonusPercentage = runes.SelectMany(p => p.Stats.Where(st => st.Type == StatType.DEFPercentage)).Sum(p => p.Value) + SetBonusHelper.GetGuardBonus(runes) ?? 0.0;

            totalBonusPercentage /= 100;

            return totalBonusDEF + this.Defense * (1 + totalBonusPercentage);
        }
    }
}
