﻿namespace RuneManager.Domain.Models
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using RuneManager.Domain.Base;
    using RuneManager.Domain.Enumerations;

    public class Rune : Entity
    {
        private List<Stat> allStats;

        private int totalAttackStats = Int32.MinValue;

        private int totalSpeedAttackStats = Int32.MinValue;

        public Rune()
        {
            this.MainStat = new Stat();
            this.BonusStat = new Stat();
            this.Stat1 = new Stat();
            this.Stat2 = new Stat();
            this.Stat3 = new Stat();
            this.Stat4 = new Stat();
        }

        public int? MonsterId { get; set; }

        public int? PendingMonsterId { get; set; }
        
        public SetType Type { get; set; }

        public int Slot { get; set; }

        public Stat MainStat { get; set; }

        public Stat BonusStat { get; set; }

        public Stat Stat1 { get; set; }

        public Stat Stat2 { get; set; }

        public Stat Stat3 { get; set; }

        public Stat Stat4 { get; set; }

        public virtual Monster Monster { get; set; }

        public virtual Monster PendingMonster { get; set; }

        public bool Locked { get; set; }

        public IList<Stat> Stats
        {
            get
            {
                if (allStats == null)
                {
                    allStats = new List<Stat>();

                    allStats.Add(MainStat);

                    if (BonusStat.Value.HasValue)
                    {
                        allStats.Add(BonusStat);
                    }

                    if (Stat1.Value.HasValue)
                    {
                        allStats.Add(Stat1);
                    }

                    if (Stat2.Value.HasValue)
                    {
                        allStats.Add(Stat2);
                    }

                    if (Stat3.Value.HasValue)
                    {
                        allStats.Add(Stat3);
                    }

                    if (Stat4.Value.HasValue)
                    {
                        allStats.Add(Stat4);
                    }
                }
                return allStats;
            }
        }

        public int TotalAttackStats
        {
            get
            {
                if (this.totalAttackStats == Int32.MinValue)
                {
                    this.totalAttackStats = 0;
                    var valuedStats = new List<StatType> { StatType.CD, StatType.ATKPercentage, StatType.CR };


                    foreach (var stat in Stats.Where(p => valuedStats.Contains(p.Type.Value)))
                    {
                        this.totalAttackStats += stat.Value.Value;
                    }

                }

                return this.totalAttackStats;
            }
        }

        public int TotalSpeedAttackStats
        {
            get
            {
                if (this.totalSpeedAttackStats == Int32.MinValue)
                {
                    this.totalSpeedAttackStats = 0;
                    var valuedStats = new List<StatType> { StatType.CD, StatType.ATKPercentage, StatType.CR, StatType.SPD };


                    foreach (var stat in Stats.Where(p => valuedStats.Contains(p.Type.Value)))
                    {
                        this.totalSpeedAttackStats += stat.Value.Value;
                    }

                }

                return this.totalSpeedAttackStats;
            }
        }

        public int TotalDesiredStats(List<StatType> statTypes)
        {
            var totalDesiredStats = 0;

            foreach (var stat in Stats.Where(p => statTypes.Contains(p.Type.Value)))
            {
                totalDesiredStats += stat.Value.Value;
            }

            return totalDesiredStats;
        }

        public void ClearStatCache()
        {
            allStats = null;
        }
    }
}
