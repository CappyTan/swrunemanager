﻿namespace RuneManager.Domain.Extensions
{
    using System.Collections.Generic;
    using System.Linq;

    using RuneManager.Domain.Enumerations;
    using RuneManager.Domain.Models;

    public static class RuneExtensions
    {
        public static int? TotalStats(this IEnumerable<Rune> runes, StatType stat)
        {
            return runes.SelectMany(p => p.Stats.Where(st => st.Type.HasValue && st.Type.Value == stat)).Sum(p => p.Value);
        }
    }
}
