﻿namespace RuneManager.Domain.Extensions
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Reflection;

    using RuneManager.Domain.Attributes;

    public static class RuneManagerEnumerationExtensions
    {
        #region Public Methods

        /// <summary>
        ///     Converts a sting into an element of the specified enum
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="enumString"></param>
        /// <returns></returns>
       public static int GetSetSize(this Enum value)
        {
            if (value == null)
            {
                throw new InvalidEnumArgumentException("Enum doesn't support the SetSize Attribute");
            }

            Type type = value.GetType();
            string name = Enum.GetName(type, value);
            if (name != null)
            {
                FieldInfo field = type.GetField(name);
                if (field != null)
                {
                    var attr = Attribute.GetCustomAttribute(field, typeof(SetSizeAttribute)) as SetSizeAttribute;
                    if (attr != null)
                    {
                        return attr.Size;
                    }
                }
            }
            
           throw new InvalidEnumArgumentException("Enum doesn't support the SetSize Attribute");
        }

        #endregion
    }
}
