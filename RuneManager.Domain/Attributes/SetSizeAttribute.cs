﻿namespace RuneManager.Domain.Attributes
{
    using System;

    public class SetSizeAttribute : Attribute
    {
        public int Size { get; set; }

        public SetSizeAttribute(int size)
        {
            this.Size = size;
        }
    }
}
