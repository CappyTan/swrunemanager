﻿namespace RuneManager.Domain.Base
{
    using System.Collections.Generic;
    using System.Linq;

    using RuneManager.Domain.Enumerations;
    using RuneManager.Domain.Helpers;
    using RuneManager.Domain.Models;
    using RuneManager.Domain.Models.ValueObjects;

    public class StatCalculator
    {
        private readonly Buildings buildings;

        private readonly bool isGuildWars;

        public StatCalculator(Buildings buildings, bool isGuildWars)
        {
            this.buildings = buildings;
            this.isGuildWars = isGuildWars;
        }

        public double GetCritMultiplier(Monster monster)
        {
            var runes = monster.Runes;

            return this.GetCritMultiplier(monster, runes.ToList());
        }

        public double GetCritMultiplier(Monster monster, IEnumerable<Rune> runes)
        {
            var totalBonusAtk = runes.SelectMany(p => p.Stats.Where(st => st.Type == StatType.ATK)).Sum(p => p.Value) + 0.0;
            var totalBonusPercentage = runes.SelectMany(p => p.Stats.Where(st => st.Type == StatType.ATKPercentage)).Sum(p => p.Value) + this.GetBonusAttackPercent(monster);
            var totalBonusCD = runes.SelectMany(p => p.Stats.Where(st => st.Type == StatType.CD)).Sum(p => p.Value) + this.GetBonusCritDamage();

            if (runes.Count(p => p.Type == SetType.Fatal) >= 4)
            {
                totalBonusPercentage += 35.0;
            }

            if (runes.Count(p => p.Type == SetType.Rage) >= 4)
            {
                totalBonusCD += 40.0;
            }

            totalBonusPercentage /= 100;
            totalBonusPercentage += (totalBonusAtk / (monster.Attack + 0.0));


            totalBonusCD += monster.CritDamage;
            totalBonusCD /= 100;


            return (1 + totalBonusPercentage.Value) * (1 + totalBonusCD.Value);
        }

        public double GetCritMultiplier(Monster monster, OptimizedStats optimizedStats)
        {
            var attackPercentage = (optimizedStats.TotalAttack / monster.Attack) + this.GetBonusAttackPercent(monster);

            var totalBonusCD = (optimizedStats.TotalCritDamage + this.GetBonusCritDamage()) / 100.0;

            return  attackPercentage * (1 + totalBonusCD);
        }

        public double GetHitMultiplier(Monster monster, IEnumerable<Rune> runes)
        {
            var critChance = runes.SelectMany(p => p.Stats.Where(st => st.Type.HasValue && st.Type.Value == StatType.CR)).Sum(p => p.Value) + SetBonusHelper.GetBladeBonus(runes) ?? 0.0;

            var totalBonusAtk = runes.SelectMany(p => p.Stats.Where(st => st.Type == StatType.ATK)).Sum(p => p.Value) ?? 0.0;
            var totalBonusPercentage = runes.SelectMany(p => p.Stats.Where(st => st.Type == StatType.ATKPercentage)).Sum(p => p.Value) + this.GetBonusAttackPercent(monster);
            var totalBonusCD = runes.SelectMany(p => p.Stats.Where(st => st.Type == StatType.CD)).Sum(p => p.Value) + this.GetBonusCritDamage();

            if (runes.Count(p => p.Type == SetType.Fatal) >= 4)
            {
                totalBonusPercentage += 35.0;
            }

            if (runes.Count(p => p.Type == SetType.Rage) >= 4)
            {
                totalBonusCD += 40.0;
            }

            totalBonusPercentage /= 100;
            totalBonusPercentage += (totalBonusAtk / (monster.Attack + 0.0));


            totalBonusCD += monster.CritDamage;
            totalBonusCD /= 100;

            var critPercent = (critChance + monster.CritChance + 0.0) / 100.0;

            if (critPercent > 1)
            {
                critPercent = 1.0;
            }

            return (1 + totalBonusPercentage.Value) * (1 + critPercent * totalBonusCD.Value);
        }

        public double GetHitMultiplier(Monster monster, OptimizedStats optimizedStats)
        {
            var attackPercentage = (optimizedStats.TotalAttack / monster.Attack) + this.GetBonusAttackPercent(monster);

            var totalBonusCD = (optimizedStats.TotalCritDamage + this.GetBonusCritDamage()) / 100.0;

            var critPercent = (optimizedStats.CritChance + monster.CritChance + 0.0) / 100.0;

            if (critPercent > 1)
            {
                critPercent = 1.0;
            }

            return attackPercentage * (1 + critPercent * totalBonusCD);
        }

        public double GetDOTMultiplier(Monster monster, IEnumerable<Rune> runes)
        {
            var damagePerHit = this.GetHitMultiplier(monster, runes);

            var speed =
                runes.SelectMany(p => p.Stats.Where(st => st.Type.HasValue && st.Type.Value == StatType.SPD))
                    .Sum(p => p.Value) + SetBonusHelper.GetSwiftBonus(monster, runes) + ((1 + this.GetBonusSpeed()) * monster.Speed) ?? monster.Speed;

            speed /= 100;

            return damagePerHit * speed;
        }

        public double GetDOTMultiplier(Monster monster, OptimizedStats optimizedStats)
        {
            var damagePerHit = this.GetHitMultiplier(monster, optimizedStats);

            var speed = optimizedStats.Speed + ((1 + this.GetBonusSpeed()) * monster.Speed);

            speed /= 100;

            return damagePerHit * speed;
        }

        public double GetHPScalingCritMultiplier(Monster monster, IEnumerable<Rune> runes)
        {
            var hpPercent = (monster.TotalHitPoints(runes) / monster.HitPoints) - 1 + 0.18;

            var totalBonusAtk = runes.SelectMany(p => p.Stats.Where(st => st.Type == StatType.ATK)).Sum(p => p.Value) ?? 0.0;
            var totalBonusPercentage = runes.SelectMany(p => p.Stats.Where(st => st.Type == StatType.ATKPercentage)).Sum(p => p.Value) + this.GetBonusAttackPercent(monster);
            var totalBonusCD = runes.SelectMany(p => p.Stats.Where(st => st.Type == StatType.CD)).Sum(p => p.Value) + this.GetBonusCritDamage();

            if (runes.Count(p => p.Type == SetType.Fatal) >= 4)
            {
                totalBonusPercentage += 35.0;
            }

            if (runes.Count(p => p.Type == SetType.Rage) >= 4)
            {
                totalBonusCD += 40.0;
            }

            totalBonusPercentage /= 100;
            totalBonusPercentage += (totalBonusAtk / (monster.Attack + 0.0));

            totalBonusCD += monster.CritDamage;
            totalBonusCD /= 100;

            return (1 + totalBonusPercentage.Value + hpPercent) * (1 + totalBonusCD.Value);
        }

        public double GetHPScalingCritMultiplier(Monster monster, OptimizedStats optimizedStats)
        {
            var hpPercent = (optimizedStats.HitPoints / monster.HitPoints) - 1 + 0.18;

            var totalBonusPercentage = (optimizedStats.TotalAttack / monster.Attack) - 1 + this.GetBonusAttackPercent(monster);

            var totalBonusCD = (optimizedStats.TotalCritDamage + this.GetBonusCritDamage()) / 100.0;
            
            return (1 + totalBonusPercentage + hpPercent) * (1 + totalBonusCD);
        }

        public double GetHPScalingHitMultiplier(Monster monster, IEnumerable<Rune> runes)
        {
            var critChance = runes.SelectMany(p => p.Stats.Where(st => st.Type.HasValue && st.Type.Value == StatType.CR)).Sum(p => p.Value) + SetBonusHelper.GetBladeBonus(runes) ?? 0.0;

            var hpPercent = (monster.TotalHitPoints(runes) / monster.HitPoints) - 1 + 0.18;

            var totalBonusAtk = runes.SelectMany(p => p.Stats.Where(st => st.Type == StatType.ATK)).Sum(p => p.Value) ?? 0.0;
            var totalBonusPercentage = runes.SelectMany(p => p.Stats.Where(st => st.Type == StatType.ATKPercentage)).Sum(p => p.Value) + this.GetBonusAttackPercent(monster);
            var totalBonusCD = runes.SelectMany(p => p.Stats.Where(st => st.Type == StatType.CD)).Sum(p => p.Value) + this.GetBonusCritDamage();

            if (runes.Count(p => p.Type == SetType.Fatal) >= 4)
            {
                totalBonusPercentage += 35.0;
            }

            if (runes.Count(p => p.Type == SetType.Rage) >= 4)
            {
                totalBonusCD += 40.0;
            }

            totalBonusPercentage /= 100;
            totalBonusPercentage += (totalBonusAtk / (monster.Attack + 0.0));


            totalBonusCD += monster.CritDamage;
            totalBonusCD /= 100;

            var critPercent = (critChance + monster.CritChance + 0.0) / 100.0;

            if (critPercent > 1)
            {
                critPercent = 1.0;
            }

            return (1 + totalBonusPercentage.Value + hpPercent) * (1 + critPercent * totalBonusCD.Value);
        }

        public double GetHPScalingHitMultiplier(Monster monster, OptimizedStats optimizedStats)
        {
            var hpPercent = (optimizedStats.HitPoints / monster.HitPoints) - 1 + 0.18;

            var totalBonusPercentage = (optimizedStats.TotalAttack / monster.Attack) - 1 + this.GetBonusAttackPercent(monster);

            var totalBonusCD = (optimizedStats.TotalCritDamage + this.GetBonusCritDamage()) / 100.0;

            var critPercent = (optimizedStats.CritChance + monster.CritChance + 0.0) / 100.0;

            if (critPercent > 1)
            {
                critPercent = 1.0;
            }

            return (1 + totalBonusPercentage + hpPercent) * (1 + critPercent * totalBonusCD);
        }

        public double GetHPScalingDOTMultiplier(Monster monster, IEnumerable<Rune> runes)
        {
            var damagePerHit = this.GetHPScalingHitMultiplier(monster, runes);

            var speed =
                runes.SelectMany(p => p.Stats.Where(st => st.Type.HasValue && st.Type.Value == StatType.SPD))
                    .Sum(p => p.Value) + SetBonusHelper.GetSwiftBonus(monster, runes) + ((1 + this.GetBonusSpeed()) * monster.Speed) ?? monster.Speed;

            speed /= 100;

            return damagePerHit * speed;
        }

        public double GetHPScalingDOTMultiplier(Monster monster, OptimizedStats optimizedStats)
        {
            var damagePerHit = this.GetHPScalingHitMultiplier(monster, optimizedStats);

            var speed = optimizedStats.Speed + ((1 + this.GetBonusSpeed()) * monster.Speed);

            speed /= 100;

            return damagePerHit * speed;
        }

        public double GetDefScalingCritMultiplier(Monster monster, IEnumerable<Rune> runes)
        {
            var defPercent = (monster.TotalDefense(runes) / monster.Defense) - 1 + 0.08;

            var totalBonusAtk = runes.SelectMany(p => p.Stats.Where(st => st.Type == StatType.ATK)).Sum(p => p.Value) ?? 0.0;
            var totalBonusPercentage = runes.SelectMany(p => p.Stats.Where(st => st.Type == StatType.ATKPercentage)).Sum(p => p.Value) + this.GetBonusAttackPercent(monster);
            var totalBonusCD = runes.SelectMany(p => p.Stats.Where(st => st.Type == StatType.CD)).Sum(p => p.Value) + this.GetBonusCritDamage();

            if (runes.Count(p => p.Type == SetType.Fatal) >= 4)
            {
                totalBonusPercentage += 35.0;
            }

            if (runes.Count(p => p.Type == SetType.Rage) >= 4)
            {
                totalBonusCD += 40.0;
            }

            totalBonusPercentage /= 100;
            totalBonusPercentage += (totalBonusAtk / (monster.Attack + 0.0));

            totalBonusCD += monster.CritDamage;
            totalBonusCD /= 100;

            return (1 + totalBonusPercentage.Value + defPercent) * (1 + totalBonusCD.Value);
        }

        public double GetDefScalingCritMultiplier(Monster monster, OptimizedStats optimizedStats)
        {
            var defPercent = (optimizedStats.TotalDefense / monster.Defense) - 1 + 0.08;

            var totalBonusPercentage = (optimizedStats.TotalAttack / monster.Attack) - 1 + this.GetBonusAttackPercent(monster);

            var totalBonusCD = (optimizedStats.TotalCritDamage + this.GetBonusCritDamage()) / 100.0;

            return (1 + totalBonusPercentage + defPercent) * (1 + totalBonusCD);
        }

        public double GetDefScalingHitMultiplier(Monster monster, IEnumerable<Rune> runes)
        {
            var critChance = runes.SelectMany(p => p.Stats.Where(st => st.Type.HasValue && st.Type.Value == StatType.CR)).Sum(p => p.Value) + SetBonusHelper.GetBladeBonus(runes) ?? 0.0;

            var defPercent = (monster.TotalDefense(runes) / monster.Defense) - 1 + 0.08;

            var totalBonusAtk = runes.SelectMany(p => p.Stats.Where(st => st.Type == StatType.ATK)).Sum(p => p.Value) ?? 0.0;
            var totalBonusPercentage = runes.SelectMany(p => p.Stats.Where(st => st.Type == StatType.ATKPercentage)).Sum(p => p.Value) + this.GetBonusAttackPercent(monster);
            var totalBonusCD = runes.SelectMany(p => p.Stats.Where(st => st.Type == StatType.CD)).Sum(p => p.Value) + this.GetBonusCritDamage();

            if (runes.Count(p => p.Type == SetType.Fatal) >= 4)
            {
                totalBonusPercentage += 35.0;
            }

            if (runes.Count(p => p.Type == SetType.Rage) >= 4)
            {
                totalBonusCD += 40.0;
            }

            totalBonusPercentage /= 100;
            totalBonusPercentage += (totalBonusAtk / (monster.Attack + 0.0));


            totalBonusCD += monster.CritDamage;
            totalBonusCD /= 100;

            var critPercent = (critChance + monster.CritChance + 0.0) / 100.0;

            if (critPercent > 1)
            {
                critPercent = 1.0;
            }

            return (1 + totalBonusPercentage.Value + defPercent) * (1 + critPercent * totalBonusCD.Value);
        }

        public double GetDefScalingHitMultiplier(Monster monster, OptimizedStats optimizedStats)
        {
            var defPercent = (optimizedStats.TotalDefense / monster.Defense) - 1 + 0.08;

            var totalBonusPercentage = (optimizedStats.TotalAttack / monster.Attack) - 1 + this.GetBonusAttackPercent(monster);

            var totalBonusCD = (optimizedStats.TotalCritDamage + this.GetBonusCritDamage()) / 100.0;

            var critPercent = (optimizedStats.CritChance + monster.CritChance + 0.0) / 100.0;

            if (critPercent > 1)
            {
                critPercent = 1.0;
            }

            return (1 + totalBonusPercentage + defPercent) * (1 + critPercent * totalBonusCD);
        }

        public double GetDefScalingDOTMultiplier(Monster monster, IEnumerable<Rune> runes)
        {
            var damagePerHit = this.GetDefScalingHitMultiplier(monster, runes);

            var speed =
                runes.SelectMany(p => p.Stats.Where(st => st.Type.HasValue && st.Type.Value == StatType.SPD))
                    .Sum(p => p.Value) + SetBonusHelper.GetSwiftBonus(monster, runes) + ((1 + this.GetBonusSpeed()) * monster.Speed) ?? monster.Speed;

            speed /= 100;

            return damagePerHit * speed;
        }

        public double GetDefScalingDOTMultiplier(Monster monster, OptimizedStats optimizedStats)
        {
            var damagePerHit = this.GetDefScalingHitMultiplier(monster, optimizedStats);

            var speed = optimizedStats.Speed + ((1 + this.GetBonusSpeed()) * monster.Speed);

            speed /= 100;

            return damagePerHit * speed;
        }

        public double GetEffectiveHealth(Monster monster, IEnumerable<Rune> runes)
        {
            var totalHP = monster.TotalHitPoints(runes) + (this.GetBonusHitPoints() * monster.HitPoints);
            var totalDEF = monster.TotalDefense(runes) + (this.GetBonusDefense() * monster.Defense);

            var mitigation = 1000.0 / (1000.0 + 3 * totalDEF);

            return totalHP / mitigation;
        }

        public double GetEffectiveHealth(Monster monster, OptimizedStats optimizedStats)
        {
            var totalHP = optimizedStats.HitPoints + (this.GetBonusHitPoints() *  monster.HitPoints);
            var totalDEF = optimizedStats.TotalDefense + (this.GetBonusDefense() * monster.Defense);

            var mitigation = 1000.0 / (1000.0 + 3 * totalDEF);

            return totalHP / mitigation;
        }

        public double GetCritCoefficient(Monster monster, OptimizedStats optimizedStats)
        {
            var totalBonusCD = (optimizedStats.TotalCritDamage + this.GetBonusCritDamage()) / 100.0;

            var critPercent = (optimizedStats.CritChance + monster.CritChance + 0.0) / 100.0;

            if (critPercent > 1)
            {
                critPercent = 1.0;
            }

            return critPercent * totalBonusCD;
        }

        public double GetAttackCoefficient(Monster monster, OptimizedStats optimizedStats)
        {
            return (optimizedStats.TotalAttack / monster.Attack) + this.GetBonusAttackPercent(monster);
        }

        private double GetBonusAttackPercent(Monster monster)
        {
            double bonus = buildings.AttackTower;
            switch (monster.Element)
            {
                case Element.Dark:
                    bonus += buildings.DarkTower;
                    break;
                case Element.Fire:
                    bonus += buildings.FireTower;
                    break;
                case Element.Light:
                    bonus += buildings.LightTower;
                    break;
                case Element.Water:
                    bonus += buildings.WaterTower;
                    break;
                case Element.Wind:
                    bonus += buildings.WindTower;
                    break;
            }

            if (isGuildWars)
            {
                bonus += buildings.AttackFlag;
            }

            return bonus / 100;
        }

        private double GetBonusCritDamage()
        {
            double bonus = buildings.CritDamageTower;

            if (this.isGuildWars)
            {
                bonus += buildings.CritDamageFlag;
            }

            return bonus;
        }

        private double GetBonusHitPoints()
        {
            double bonus = buildings.HitPointTower;

            if (this.isGuildWars)
            {
                bonus += buildings.HitPointFlag;
            }

            return bonus / 100;
        }

        private double GetBonusDefense()
        {
            double bonus = buildings.DefenseTower;

            if (this.isGuildWars)
            {
                bonus += buildings.DefenseFlag;
            }

            return bonus / 100;
        }

        private double GetBonusSpeed()
        {
            double bonus = buildings.SpeedTower;

            return bonus / 100;
        }
    }


}

