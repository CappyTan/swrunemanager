namespace RuneManager.Domain.Base
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Linq;

    using RuneManager.Domain.Contracts.Repositories;
    using RuneManager.Domain.Enumerations;
    using RuneManager.Domain.Extensions;
    using RuneManager.Domain.Helpers;
    using RuneManager.Domain.Models;
    using RuneManager.Domain.Models.ValueObjects;

    public abstract class OptimizerBase
    {
        private readonly IBuildingsRepository buildingsRepository;

        private StatCalculator critCalc;

        protected OptimizerParameters parameters;

        long totalCombinations = 0;
        long currentCombination = 0;

        private DateTime lastReportedTime = DateTime.MinValue;

        public OptimizerBase(IBuildingsRepository buildingsRepository)
        {
            this.buildingsRepository = buildingsRepository;
        }

        public IOrderedEnumerable<RuneSet> FindSets(Monster monster, IList<Rune> runes, OptimizerParameters optimizerParameters, BackgroundWorker worker)
        {
            critCalc = new StatCalculator(this.buildingsRepository.GetBuildings(), optimizerParameters.IsGuildWars);

            totalCombinations = 0;
            currentCombination = 0;
            IList<SetType> twoSetTypes= new List<SetType>();
            IList<IGrouping<SetType, SetType>> groups = null;

            this.parameters = optimizerParameters;

            var runeSets = new List<RuneSet>();

            if (parameters.FourSet.HasValue && parameters.TwoSet.HasValue)
            {
                runes = runes.Where(p => p.Type == parameters.FourSet.Value || p.Type == parameters.TwoSet.Value).ToList();
            }
            else if (parameters.TwoSet.HasValue && parameters.TwoSet2.HasValue && parameters.TwoSet3.HasValue)
            {
                runes = runes.Where(p => p.Type == parameters.TwoSet.Value || p.Type == parameters.TwoSet2.Value || p.Type == parameters.TwoSet3.Value).ToList();
            }

            if (parameters.TwoSet.HasValue)
            {
                twoSetTypes.Add(parameters.TwoSet.Value);
            }

            if (parameters.TwoSet2.HasValue)
            {
                twoSetTypes.Add(parameters.TwoSet2.Value);
            }

            if (parameters.TwoSet3.HasValue)
            {
                twoSetTypes.Add(parameters.TwoSet3.Value);
            }
           

            groups = twoSetTypes.GroupBy(p => p).ToList();

            runes = runes.Where(p => !p.Locked && p.PendingMonster == null).ToList();

            if (optimizerParameters.OptimizerSpeed != OptimizerSpeedType.Full)
            {
                runes = this.FilterOutIrrelevantRunes(monster, runes, optimizerParameters.OptimizerSpeed);
            }

            if (optimizerParameters.IgnoreEquipped)
            {
                runes = runes.Where(p => p.MonsterId == monster.Id || !p.MonsterId.HasValue).ToList();
            }
            
            

            var slot1 = runes.Where(p => p.Slot == 1).ToList();
            var slot2 = runes.Where(p => p.Slot == 2).ToList();
            var slot3 = runes.Where(p => p.Slot == 3).ToList();
            var slot4 = runes.Where(p => p.Slot == 4).ToList();
            var slot5 = runes.Where(p => p.Slot == 5).ToList();
            var slot6 = runes.Where(p => p.Slot == 6).ToList();

            long slot1Count = slot1.Count;
            long slot2Count = slot2.Count;
            long slot3Count = slot3.Count;
            long slot4Count = slot4.Count;
            long slot5Count = slot5.Count;
            long slot6Count = slot6.Count;


            totalCombinations = slot1Count * slot2Count * slot3Count * slot4Count * slot5Count * slot6Count;

            this.SendUpdate(worker);

            var runeSetStack = new Stack<Rune>();

            foreach (var slot1Rune in slot1)
            {
                runeSetStack.Push(slot1Rune);

                foreach (var slot2Rune in slot2)
                {
                    runeSetStack.Push(slot2Rune);

                    foreach (var slot3Rune in slot3)
                    {
                        runeSetStack.Push(slot3Rune);

                        if (parameters.FourSet.HasValue && runeSetStack.Count(p => p.Type == parameters.FourSet.Value) == 0)
                        {
                            currentCombination += slot4.Count * slot5.Count * slot6.Count;

                            this.SendUpdate(worker);
                            runeSetStack.Pop();
                            continue;
                        }
                        else if (twoSetTypes.Count == 3)
                        {
                            bool invalid = false;


                            foreach (var group in groups)
                            {
                                if (runeSetStack.Count(p => p.Type == group.Key) > 2 * group.Count())
                                {
                                    invalid = true;
                                }
                            }

                            if (invalid)
                            {
                                currentCombination += slot4.Count * slot5.Count * slot6.Count;

                                this.SendUpdate(worker);
                                runeSetStack.Pop();
                                continue;
                            }
                        }
                        else if (twoSetTypes.Count == 2)
                        {
                            bool invalid = false;
                            
                            foreach (var group in groups)
                            {
                                if (runeSetStack.Count(p => p.Type == group.Key) == 0)
                                {
                                    invalid = true;
                                }
                            }

                            if (invalid)
                            {
                                currentCombination += slot4.Count * slot5.Count * slot6.Count;

                                this.SendUpdate(worker);
                                runeSetStack.Pop();
                                continue;
                            }

                        }


                        foreach (var slot4Rune in slot4)
                        {
                            runeSetStack.Push(slot4Rune);

                            if (parameters.FourSet.HasValue && runeSetStack.Count(p => p.Type == parameters.FourSet.Value) < 2)
                            {
                                currentCombination += slot5.Count * slot6.Count;
                                this.SendUpdate(worker);
                                runeSetStack.Pop();
                                continue;
                            }
                            else if (twoSetTypes.Count == 3)
                            {
                                bool invalid = false;
                                foreach (var group in groups)
                                {
                                    if (runeSetStack.Count(p => p.Type == group.Key) > 2 * group.Count())
                                    {
                                        invalid = true;
                                    }
                                }

                                if (invalid)
                                {
                                    currentCombination += slot5.Count * slot6.Count;

                                    this.SendUpdate(worker);
                                    runeSetStack.Pop();
                                    continue;
                                }
                            }
                            else if (twoSetTypes.Count == 2)
                            {
                                var count = 0;
                                foreach (var group in groups)
                                {
                                    count += runeSetStack.Count(p => p.Type == group.Key);
                                }

                                if (count == 0)
                                {
                                    currentCombination += slot5.Count * slot6.Count;

                                    this.SendUpdate(worker);
                                    runeSetStack.Pop();
                                    continue;
                                }

                            }


                            foreach (var slot5Rune in slot5)
                            {
                                runeSetStack.Push(slot5Rune);

                                if ((parameters.FourSet.HasValue && runeSetStack.Count(p => p.Type == parameters.FourSet.Value) < 3) || (parameters.TwoSet.HasValue && runeSetStack.Count(p => p.Type == parameters.TwoSet.Value) == 0))
                                {
                                    currentCombination += slot6.Count;
                                    this.SendUpdate(worker);
                                    runeSetStack.Pop();
                                    continue;
                                }
                                else if (twoSetTypes.Count == 3)
                                {
                                    bool invalid = false;
                                    foreach (var group in groups)
                                    {
                                        if (runeSetStack.Count(p => p.Type == group.Key) > 2 * group.Count())
                                        {
                                            invalid = true;
                                        }
                                    }

                                    if (invalid)
                                    {
                                        currentCombination += slot6.Count;

                                        this.SendUpdate(worker);
                                        runeSetStack.Pop();
                                        continue;
                                    }
                                }
                                else if (twoSetTypes.Count == 2)
                                {
                                    bool invalid = false;
                                    bool madeGroup = false;
                                    foreach (var group in groups)
                                    {
                                        if (group.Count() == 2 && runeSetStack.Count(p => p.Type == group.Key) < 3)
                                        {
                                            invalid = true;
                                        }
                                        else if (group.Count() == 1 && runeSetStack.Count(p => p.Type == group.Key) == 0)
                                        {
                                            invalid = true;
                                        }

                                        if (runeSetStack.Count(p => p.Type == group.Key) >= 2)
                                        {
                                            madeGroup = true;
                                        }
                                    }

                                    if (invalid || !madeGroup)
                                    {
                                        currentCombination += slot6.Count;

                                        this.SendUpdate(worker);
                                        runeSetStack.Pop();
                                        continue;
                                    }
                                    
                                }

                                foreach (var slot6Rune in slot6)
                                {
                                    runeSetStack.Push(slot6Rune);
                                    currentCombination++;
                                    this.SendUpdate(worker);

                                    if ((parameters.FourSet.HasValue && runeSetStack.Count(p => p.Type == parameters.FourSet.Value) < 4) || (parameters.TwoSet.HasValue && runeSetStack.Count(p => p.Type == parameters.TwoSet.Value) < 2))
                                    {
                                        runeSetStack.Pop();
                                        continue;
                                    }
                                    else if (twoSetTypes.Count == 3)
                                    {
                                        bool invalid = false;
                                        foreach (var group in groups)
                                        {
                                            if (runeSetStack.Count(p => p.Type == group.Key) > 2 * group.Count())
                                            {
                                                invalid = true;
                                            }
                                        }

                                        if (invalid)
                                        {
                                            runeSetStack.Pop();
                                            continue;
                                        }
                                    }
                                    else if (twoSetTypes.Count == 2)
                                    {
                                        bool invalid = false;
                                        
                                        foreach (var group in groups)
                                        {
                                            if (group.Count() == 2 && runeSetStack.Count(p => p.Type == group.Key) < 4)
                                            {
                                                invalid = true;
                                            }
                                            else if (group.Count() == 1 && runeSetStack.Count(p => p.Type == group.Key) < 2)
                                            {
                                                invalid = true;
                                            }
                                        }

                                        if (invalid)
                                        {
                                            runeSetStack.Pop();
                                            continue;
                                        }

                                    }

                                    var optimizedStats = this.GetOptimizedStats(monster, runeSetStack);

                                    if (this.IsValidRuneSet(monster, optimizedStats))
                                    {
                                        this.AddCalculatedOptmizedStats(monster, runeSetStack, optimizedStats);
                                        this.AddRuneToRuneSets(monster, runeSetStack, optimizedStats, runeSets);
                                    }

                                    runeSetStack.Pop();
                                }
                                runeSetStack.Pop();
                            }
                            runeSetStack.Pop();
                        }
                        runeSetStack.Pop();
                    }
                    runeSetStack.Pop();
                }
                runeSetStack.Pop();
            }

            worker.ReportProgress(0, new OptimizerProgress { CurrentCombination = currentCombination, TotalCombinations = totalCombinations });
            return runeSets.OrderByDescending(this.OptimizedVariablePredicate());
        }

        private void SendUpdate(BackgroundWorker worker)
        {
            if ((DateTime.Now - lastReportedTime).TotalSeconds > 1)
            {
                worker.ReportProgress(0, new OptimizerProgress { CurrentCombination = currentCombination, TotalCombinations = totalCombinations });
                lastReportedTime = DateTime.Now;
            }
            
        }

        private bool IsValidRuneSet(Monster monster, OptimizedStats optimizedStats)
        {
            var minCritChance = parameters.MinCritChance - monster.CritChance;
            var maxCritChance = parameters.MaxCritChance - monster.CritChance;
            var minSpeed = parameters.MinSpeed - monster.Speed;
            var maxSpeed = parameters.MaxSpeed.HasValue ? parameters.MaxSpeed - monster.Speed : Int32.MaxValue;
            var minAccuracy = parameters.MinAccuracy - monster.Accuracy;
            var minResistance = parameters.MinResistance - monster.Resistance;

            if (optimizedStats.CritChance < minCritChance || optimizedStats.CritChance > maxCritChance)
            {
                return false;
            }

            if (optimizedStats.Speed < minSpeed || optimizedStats.Speed > maxSpeed)
            {
                return false;
            }

            if (optimizedStats.Accuracy < minAccuracy)
            {
                return false;
            }

            if (optimizedStats.Resistance < minResistance)
            {
                return false;
            }

            if (optimizedStats.HitPoints < parameters.MinHitPoints)
            {
                return false;
            }

            if (optimizedStats.Defense < parameters.MinDefense)
            {
                return false;
            }

            if (optimizedStats.EffectiveHitPoints < parameters.MinEffectiveHitPoints)
            {
                return false;
            }

            



            return true;
        }

        protected abstract IList<Rune> FilterOutIrrelevantRunes(Monster monster, IList<Rune> runes, OptimizerSpeedType optimizerSpeed);

        public abstract string Description { get; }

        public abstract string Examples { get; }

        protected void AddRuneToRuneSets(
            Monster monster,
            Stack<Rune> runeSetStack,
            OptimizedStats optimizedStats,
            List<RuneSet> runeSets)
        {
            double critX, hitX, DOTX;
            if (parameters.ScalingType == ScalingType.HitPoints)
            {
                critX = critCalc.GetHPScalingCritMultiplier(monster, optimizedStats);
                hitX = critCalc.GetHPScalingHitMultiplier(monster, optimizedStats);
                DOTX = critCalc.GetHPScalingDOTMultiplier(monster, optimizedStats);
            }
            else if (parameters.ScalingType == ScalingType.Defense)
            {
                critX = critCalc.GetDefScalingCritMultiplier(monster, optimizedStats);
                hitX = critCalc.GetDefScalingHitMultiplier(monster, optimizedStats);
                DOTX = critCalc.GetDefScalingDOTMultiplier(monster, optimizedStats);
            }
            else
            {
                critX = critCalc.GetCritMultiplier(monster, optimizedStats);
                hitX = critCalc.GetHitMultiplier(monster, optimizedStats);
                DOTX = critCalc.GetDOTMultiplier(monster, optimizedStats);
            }

            double critCoefficient = critCalc.GetCritCoefficient(monster, optimizedStats);
            
            runeSets.Add(new RuneSet
                {
                    Runes = runeSetStack.Reverse().ToList(),
                    TotalCrit = optimizedStats.CritChance,
                    TotalSpeed = optimizedStats.Speed,
                    TotalAccuracy = optimizedStats.Accuracy,
                    TotalResistance = optimizedStats.Resistance,
                    CritMultiplier = critX,
                    HitMultiplier = hitX,
                    DOTMultiplier = DOTX,
                    EffectiveHealth = optimizedStats.EffectiveHitPoints,
                    EffectiveRaidFrontLineHealth = critCalc.GetRaidFrontLineEffectiveHealth(monster, optimizedStats),
                    TotalHitPoints = optimizedStats.HitPoints,
                    TotalDefense = optimizedStats.TotalDefense,
                    CritCoefficient = critCoefficient,
                    AttackCoefficient = critCalc.GetAttackCoefficient(monster, optimizedStats)
                });

            if (runeSets.Count > 30)
            {
                runeSets.Remove(runeSets.OrderBy(this.OptimizedVariablePredicate()).First());
            }
        }

        protected abstract Func<RuneSet, double> OptimizedVariablePredicate();

        protected OptimizedStats GetOptimizedStats(Monster monster, Stack<Rune> runeSetStack)
        {
            var stats = new OptimizedStats();

            stats.Speed = runeSetStack.TotalStats(StatType.SPD) + SetBonusHelper.GetSwiftBonus(monster, runeSetStack) ?? monster.Speed;

            stats.Resistance = runeSetStack.TotalStats(StatType.RES) + SetBonusHelper.GetEndureBonus(runeSetStack) ?? monster.Defense;

            stats.CritChance = runeSetStack.TotalStats(StatType.CR) + SetBonusHelper.GetBladeBonus(runeSetStack) ?? monster.CritChance;

            stats.Accuracy = runeSetStack.TotalStats(StatType.ACC) + SetBonusHelper.GetFocusBonus(runeSetStack) ?? monster.Accuracy;

            stats.HitPoints = monster.TotalHitPoints(runeSetStack);

            stats.Defense = monster.TotalDefense(runeSetStack);

            if (parameters.MinEffectiveHitPoints > 0)
            {
                stats.EffectiveHitPoints = this.critCalc.GetEffectiveHealth(monster, runeSetStack);
            }

            

            return stats;
        }

        protected void AddCalculatedOptmizedStats(
            Monster monster,
            Stack<Rune> runeSetStack,
            OptimizedStats optimizedStats)
        {
            optimizedStats.TotalAttack = monster.TotalAttack(runeSetStack);
            optimizedStats.TotalCritDamage = monster.TotalCritDamage(runeSetStack) + parameters.SkillUp;
            optimizedStats.TotalDefense = monster.TotalDefense(runeSetStack);

            if (parameters.MinEffectiveHitPoints == 0)
            {
                optimizedStats.EffectiveHitPoints = this.critCalc.GetEffectiveHealth(monster, runeSetStack);
            }
        }

        

        protected IList<Rune> FilterTop20PerSlot(
            IList<Rune> runes,
            OptimizerParameters parameters,
            int[] slots,
            List<StatType> mainStats,
            List<StatType> desiredStats)
        {
            runes = runes.Where(p => slots.Contains(p.Slot) || mainStats.Contains(p.MainStat.Type.Value)).ToList();

            /*var runesBySlot = runes.GroupBy(p => p.Slot);

            foreach (var group in runesBySlot)
            {
                if (@group.Count() > 20)
                {
                    var goodRunes = new List<Rune>();
                    if (parameters.FourSet.HasValue)
                    {
                        goodRunes.AddRange(@group.Where(p => p.Type == parameters.FourSet.Value));
                    }

                    if (parameters.TwoSet.HasValue)
                    {
                        goodRunes.AddRange(@group.Where(p => p.Type == parameters.TwoSet.Value));
                    }

                    if (goodRunes.Any())
                    {
                        var worstRune = goodRunes.Min(p => p.TotalDesiredStats(desiredStats));
                        goodRunes.AddRange(
                            @group.Where(p => !goodRunes.Contains(p) && p.TotalDesiredStats(desiredStats) > worstRune));
                        goodRunes = goodRunes.OrderByDescending(p => p.TotalDesiredStats(desiredStats)).Take(20).ToList();
                    }
                    else
                    {
                        goodRunes.AddRange(@group.Take(20).OrderByDescending(p => p.TotalDesiredStats(desiredStats)));
                    }

                    if (parameters.FourSet.HasValue)
                    {
                        if (goodRunes.Count(p => p.Type == parameters.FourSet.Value) < 5)
                        {
                            goodRunes.AddRange(
                                @group.Where(p => p.Type == parameters.FourSet.Value).OrderByDescending(p => p.TotalDesiredStats(desiredStats)).Take(5)
                                    );
                        }
                    }

                    if (parameters.TwoSet.HasValue)
                    {
                        if (goodRunes.Count(p => p.Type == parameters.TwoSet.Value) < 5)
                        {
                            goodRunes.AddRange(
                                @group.Where(p => p.Type == parameters.TwoSet.Value).OrderByDescending(p => p.TotalDesiredStats(desiredStats)).Take(5)
                                    );
                        }
                    }

                    foreach (var rune in @group)
                    {
                        if (!goodRunes.Contains(rune))
                        {
                            runes.Remove(rune);
                        }
                    }
                }
            }*/
            return runes;
        }
    }
}