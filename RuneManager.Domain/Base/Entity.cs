﻿namespace RuneManager.Domain.Base
{
    public abstract class Entity
    {
        public int Id { get; set; }
    }
}
