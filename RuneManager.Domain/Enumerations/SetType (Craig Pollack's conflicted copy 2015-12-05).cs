﻿namespace RuneManager.Domain.Enumerations
{
    using RuneManager.Domain.Attributes;

    public enum SetType
    {
        [SetSize(4)]
        Fatal,

        [SetSize(2)]
        Blade,

        [SetSize(2)]
        Nemesis,

        [SetSize(4)]
        Violent,

        [SetSize(4)]
        Rage,

        [SetSize(2)]
        Shield,

        [SetSize(4)]
        Swift,

        [SetSize(2)]
        Guard,

        [SetSize(2)]
        Energy,

        [SetSize(2)]
        Focus,

        [SetSize(2)]
        Revenge,

        [SetSize(4)]
        Despair,

        [SetSize(2)]
        Endure,

        [SetSize(4)]
        Vampire,

        [SetSize(2)]
        Will
    }
}
