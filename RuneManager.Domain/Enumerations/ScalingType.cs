﻿namespace RuneManager.Domain.Enumerations
{
    public enum ScalingType
    {
        Attack,
        Defense,
        HitPoints
    }
}
