﻿namespace RuneManager.Domain.Enumerations
{
    public enum OptimizerSpeedType
    {
        Smart,
        Full,
        Fast
    }
}
