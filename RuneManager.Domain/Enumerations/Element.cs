﻿namespace RuneManager.Domain.Enumerations
{
    using System.ComponentModel;

    public enum Element
    {
        Wind,
        Water,
        Fire,
        Light,
        Dark
    }
}
