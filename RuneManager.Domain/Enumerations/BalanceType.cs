﻿namespace RuneManager.Domain.Enumerations
{
    public enum BalanceType
    {
        Offensive,
        Balanced,
        Defense
    }
}
