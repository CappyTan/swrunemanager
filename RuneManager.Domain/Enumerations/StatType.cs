﻿namespace RuneManager.Domain.Enumerations
{
    using System.ComponentModel;

    public enum StatType
    {
        [Description("ATK")]
        ATK,
        [Description("ATK%")]
        ATKPercentage,
        [Description("DEF")]
        DEF,
        [Description("DEF%")]
        DEFPercentage,
        [Description("HP")]
        HP,
        [Description("HP%")]
        HPPercentage,
        [Description("CRI Rate")]
        CR,
        [Description("CRI DMG")]
        CD,
        [Description("Accuracy")]
        ACC,
        [Description("Resistance")]
        RES,
        [Description("SPD")]
        SPD
    }
}
