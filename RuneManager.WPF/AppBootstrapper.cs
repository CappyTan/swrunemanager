﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RuneManager.WPF
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Windows;

    using Caliburn.Micro;

    using RuneManager.Domain.Contracts.Repositories;
    using RuneManager.Domain.Contracts.Services;
    using RuneManager.Infrastructure.Database;
    using RuneManager.Infrastructure.Repositories;
    using RuneManager.Services;
    using RuneManager.WPF.Queries.Buildings;
    using RuneManager.WPF.Queries.Monsters;
    using RuneManager.WPF.Queries.Runes;
    using RuneManager.WPF.ViewModels;
    using RuneManager.WPF.ViewModels.Layout;

    public class AppBootstrapper : Caliburn.Micro.BootstrapperBase
    {
        SimpleContainer container;

        public AppBootstrapper()
        {
             this.Initialize();
        }

        protected override void Configure()
        {
            base.Configure();

            container = new SimpleContainer();

            container.Singleton<IWindowManager, WindowManager>();
            container.Singleton<IEventAggregator, EventAggregator>();
            container.Singleton<RuneManagerContext, RuneManagerContext>();
            container.PerRequest<IShell, ShellViewModel>();

            container.PerRequest<IRuneRepository, RuneEntityRepository>();
            container.PerRequest<IRuneService, RuneService>();
            container.PerRequest<IRuneQueries, RuneQueries>();

            container.PerRequest<IMonsterRepository, MonsterEntityRepository>();
            container.PerRequest<IMonsterService, MonsterService>();
            container.PerRequest<IMonsterQueries, MonsterQueries>();

            container.PerRequest<IBuildingsRepository, BuildingsRepository>();
            container.PerRequest<IBuildingsService, BuildingsService>();
            container.PerRequest<IBuildingsQueries, BuildingsQueries>();
        }

        protected override object GetInstance(Type service, string key)
        {
            var instance = container.GetInstance(service, key);
            if (instance != null)
                return instance;

            throw new InvalidOperationException("Could not locate any instances.");
        }

        protected override IEnumerable<object> GetAllInstances(Type service)
        {
            return container.GetAllInstances(service);
        }

        protected override void BuildUp(object instance)
        {
            container.BuildUp(instance);
        }

        protected override void OnStartup(object sender, StartupEventArgs e)
        {
            DisplayRootViewFor<IShell>();
        }
    }
}
