﻿namespace RuneManager.WPF.Queries.Buildings
{
    using RuneManager.Domain.Models;

    public interface IBuildingsQueries
    {
        Buildings GetBuildings();
    }
}