﻿namespace RuneManager.WPF.Queries.Buildings
{
    using System.Linq;

    using RuneManager.Domain.Contracts.Repositories;
    using RuneManager.Domain.Models;

    public class BuildingsQueries : IBuildingsQueries
    {
        private readonly IBuildingsRepository buildingsRepository;

        public BuildingsQueries(IBuildingsRepository buildingsRepository)
        {
            this.buildingsRepository = buildingsRepository;
        }

        public Buildings GetBuildings()
        {
            return this.buildingsRepository.GetBuildings();
        }
    }
}