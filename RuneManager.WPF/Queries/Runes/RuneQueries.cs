﻿namespace RuneManager.WPF.Queries.Runes
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.Remoting.Messaging;

    using RuneManager.Domain.Contracts.Repositories;
    using RuneManager.Domain.Enumerations;
    using RuneManager.Domain.Models;
    using RuneManager.Services;
    using RuneManager.Services.Calculators;

    using WellPoint.Marketing.CampaignTracker.Framework.Extensions;

    public class RuneQueries : IRuneQueries
    {
        private readonly IRuneRepository runeRepository;

        public RuneQueries(IRuneRepository runeRepository)
        {
            this.runeRepository = runeRepository;
        }

        public IList<RuneListItem> GetRuneList()
        {
            var runes = this.runeRepository.GetRunes();

            return GetRuneListItems(runes);
        }

        public IList<RuneListItem> GetRuneList(IList<Rune> runes)
        {
            return GetRuneListItems(runes);
        }

        private static IList<RuneListItem> GetRuneListItems(IList<Rune> runes)
        {
            var model = new List<RuneListItem>();
            var calc = new TotalStatsCalculator();
            foreach (var rune in runes)
            {
                var item = new RuneListItem();

                item.Id = rune.Id;
                item.Slot = rune.Slot;
                item.Set = rune.Type;
                item.Rune = rune;
                item.HPPercentage = GetStat(rune, StatType.HPPercentage);
                item.DEFPercentage = GetStat(rune, StatType.DEFPercentage);
                    //rune.Stats.Where(p => p.Type == StatType.DEFPercentage).Sum(p => p.Value);
                item.ATKPercentage = GetStat(rune, StatType.ATKPercentage);
                    //rune.Stats.Where(p => p.Type == StatType.ATKPercentage).Sum(p => p.Value);
                item.HP = GetStat(rune, StatType.HP); //rune.Stats.Where(p => p.Type == StatType.HP).Sum(p => p.Value);
                item.DEF = GetStat(rune, StatType.DEF); //rune.Stats.Where(p => p.Type == StatType.DEF).Sum(p => p.Value);
                item.ATK = GetStat(rune, StatType.ATK); //rune.Stats.Where(p => p.Type == StatType.ATK).Sum(p => p.Value);
                item.SPD = GetStat(rune, StatType.SPD); //rune.Stats.Where(p => p.Type == StatType.SPD).Sum(p => p.Value);
                item.ACC = GetStat(rune, StatType.ACC); //rune.Stats.Where(p => p.Type == StatType.ACC).Sum(p => p.Value);
                item.RES = GetStat(rune, StatType.RES); //rune.Stats.Where(p => p.Type == StatType.RES).Sum(p => p.Value);
                item.CR = GetStat(rune, StatType.CR); //rune.Stats.Where(p => p.Type == StatType.CR).Sum(p => p.Value);
                item.CD = GetStat(rune, StatType.CD); //rune.Stats.Where(p => p.Type == StatType.CD).Sum(p => p.Value);

                item.TotalStats = calc.Calculate(rune);
                item.Monster = rune.Monster != null ? rune.Monster.Name : string.Empty;
                item.Pending = rune.PendingMonster != null ? rune.PendingMonster.Name : String.Empty;

                model.Add(item);
            }

            return model.OrderByDescending(p => p.TotalStats).ToList();
        }

        private static int? GetStat(Rune rune, StatType statType)
        {
            var value = rune.Stats.Where(p => p.Type == statType).Sum(p => p.Value);

            if (value.HasValue && value.Value > 0)
            {
                return value;
            }

            return new int?();
        }

        public void UpdateWeights(IList<RuneListItem> runes, decimal? HPWeight, decimal? DEFWeight, decimal? ATKWeight, decimal? RESWeight, decimal? ACCWeight, decimal? SPDWeight, decimal? CRWeight, decimal? CDWeight)
        {
            var calc = new TotalStatsCalculator(HPWeight, DEFWeight, ATKWeight, RESWeight, ACCWeight, SPDWeight, CRWeight, CDWeight);

            foreach (var rune in runes)
            {
                rune.TotalStats = calc.Calculate(rune.Rune);
            }
        }

        private string BuildValue(Stat stat)
        {
            if (!stat.Type.HasValue)
            {
                return string.Empty;
            }

            var percentageArray = new[] { StatType.ACC, StatType.ATKPercentage, StatType.CD, StatType.CR, StatType.RES, StatType.DEFPercentage, StatType.HPPercentage };
            string percentageLabel = string.Empty;

            if (percentageArray.Contains(stat.Type.Value))
            {
                percentageLabel = "%";
            }

            return string.Format("{0}{1}", stat.Value, percentageLabel);
        }
    }
}
