﻿namespace RuneManager.WPF.Queries.Runes
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    using RuneManager.Domain.Enumerations;
    using RuneManager.Domain.Helpers;
    using RuneManager.Domain.Models;
    using RuneManager.WPF.Annotations;
    using RuneManager.WPF.Code;

    public class RuneListItem
    {
        private static readonly ObservableCollection<ComboBoxItem<SetType>> twoSetTypeList = new ObservableCollection<ComboBoxItem<SetType>>(ComboBoxHelpers.GetTwoSetList()); 

        public RuneListItem()
        {
            
        }

        public int Id { get; set; }

        public SetType Set { get; set; }

        public int Slot { get; set; }

        public int? HPPercentage { get; set; }

        public int? DEFPercentage { get; set; }

        public int? ATKPercentage { get; set; }

        public int? HP { get; set; }

        public int? DEF { get; set; }

        public int? ATK { get; set; }

        public int? ACC { get; set; }

        public int? RES { get; set; }

        public int? SPD { get; set; }

        public int? CD { get; set; }

        public int? CR { get; set; }

        public decimal TotalStats { get; set; }

        public Rune Rune { get; set; }

        public string Monster { get; set; }

        public string Pending { get; set; }

        public ComboBoxItem<SetType> TwoSetType { get; set; }

        public static ObservableCollection<ComboBoxItem<SetType>> TwoSetTypeList
        {
            get
            {
                return twoSetTypeList;
            }
        }
    }
}
