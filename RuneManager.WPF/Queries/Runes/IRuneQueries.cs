﻿namespace RuneManager.WPF.Queries.Runes
{
    using System.Collections.Generic;

    using RuneManager.Domain.Models;

    public interface IRuneQueries
    {
        IList<RuneListItem> GetRuneList();

        IList<RuneListItem> GetRuneList(IList<Rune> runes);

        void UpdateWeights(IList<RuneListItem> runes, decimal? HPWeight, decimal? DEFWeight, decimal? ATKWeight, decimal? RESWeight, decimal? ACCWeight, decimal? SPDWeight, decimal? CRWeight, decimal? CDWeight);
    }
}
