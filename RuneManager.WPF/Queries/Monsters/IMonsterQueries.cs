﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RuneManager.WPF.Queries.Monsters
{
    using RuneManager.Domain.Models;

    public interface IMonsterQueries
    {
        IList<Monster> GetMonsters();
    }
}
