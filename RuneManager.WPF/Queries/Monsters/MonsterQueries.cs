﻿namespace RuneManager.WPF.Queries.Monsters
{
    using System.Collections.Generic;

    using RuneManager.Domain.Contracts.Repositories;
    using RuneManager.Domain.Models;

    public class MonsterQueries : IMonsterQueries
    {
        private readonly IMonsterRepository monsterRepository;

        public MonsterQueries(IMonsterRepository monsterRepository)
        {
            this.monsterRepository = monsterRepository;
        }

        public IList<Monster> GetMonsters()
        {
            return this.monsterRepository.GetMonsters();
        }
    }
}
