﻿namespace RuneManager.WPF.Queries.Monsters
{
    using RuneManager.Domain.Models;

    public class MonsterListItem : Monster
    {
        public decimal TotalStrength { get; set; }

        public double CritMultiplier { get; set; }

        public double? PendingCritMultiplier { get; set; }

        public double HitMultiplier { get; set; }

        public double? PendingHitMultiplier { get; set; }

        public double DOTMultiplier { get; set; }

        public double? PendingDOTMultiplier { get; set; }

        public double EHP { get; set; }

        public double? PendingEHP { get; set; }

        public double HP { get; set; }

        public Monster Monster { get; set; }

        public double TotalSpeed { get; set; }

        public bool Clean { get; set; }
    }
}
