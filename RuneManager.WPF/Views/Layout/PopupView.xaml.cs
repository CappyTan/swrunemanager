﻿namespace RuneManager.WPF.Views.Layout
{
    using System.Windows.Controls;

    /// <summary>
    /// Interaction logic for PopupView.xaml
    /// </summary>
    public partial class PopupView : UserControl
    {
        public PopupView()
        {
            InitializeComponent();
        }
    }
}
