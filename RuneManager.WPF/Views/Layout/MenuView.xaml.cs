﻿namespace RuneManager.WPF.Views.Layout
{
    using System.Windows.Controls;

    /// <summary>
    /// Interaction logic for MenuView.xaml
    /// </summary>
    public partial class MenuView : UserControl
    {
        public MenuView()
        {
            InitializeComponent();
        }
    }
}
