﻿namespace RuneManager.WPF.Views.Layout
{
    using System.Windows.Controls;

    /// <summary>
    /// Interaction logic for ConfirmationView.xaml
    /// </summary>
    public partial class ConfirmationView : UserControl
    {
        public ConfirmationView()
        {
            InitializeComponent();
        }
    }
}
