﻿namespace RuneManager.WPF.ViewModels.Monsters
{
    using System.Collections.Generic;
    using System.Linq;

    using Caliburn.Micro;

    using Omu.ValueInjecter;

    using RuneManager.Domain.Base;
    using RuneManager.Domain.Contracts.Repositories;
    using RuneManager.Domain.Models;
    using RuneManager.Services.Calculators;
    using RuneManager.WPF.Messages;
    using RuneManager.WPF.Queries.Monsters;
    using RuneManager.WPF.ViewModels.Layout;

    public class MonsterListViewModel : Screen
    {
        private readonly IMonsterQueries monsterQueries;

        private readonly IMonsterRepository monsterRepository;

        private readonly IBuildingsRepository buildingsRepository;

        private readonly IEventAggregator eventAggregator;

        private readonly IWindowManager windowManager;

        private IList<MonsterListItem> list = new List<MonsterListItem>();

        public MonsterListViewModel(IMonsterQueries monsterQueries, IMonsterRepository monsterRepository, IBuildingsRepository buildingsRepository, IEventAggregator eventAggregator, IWindowManager windowManager)
        {
            this.monsterQueries = monsterQueries;
            this.monsterRepository = monsterRepository;
            this.buildingsRepository = buildingsRepository;
            this.eventAggregator = eventAggregator;
            this.windowManager = windowManager;
        }

        public IList<MonsterListItem> List
        {
            get
            {
                return this.list;
            }
        }

        public void SelectRuneSet(MonsterListItem item)
        {
            item.Monster.PendingRunes = item.Runes;
            monsterRepository.Update(item.Monster);
        }

        public void EditMonster(MonsterListItem item)
        {
            this.eventAggregator.PublishOnUIThread(new FormViewChangeMessage(item.Id, "MonsterForm"));
        }

        public void DeleteMonster(MonsterListItem item)
        {
            var model = new ConfirmationViewModel("Are you sure you wish to delete this monster?");
            this.windowManager.ShowDialog(model);

            if (model.Confirmed)
            {
                this.monsterRepository.Delete(item.Monster);
                this.list.Remove(item);
                list = list.OrderByDescending(p => p.TotalStrength).ToList();
                NotifyOfPropertyChange(() => List);
            }
        }

        protected override void OnViewLoaded(object view)
        {
            base.OnViewLoaded(view);
            var calculator = new TotalStatsCalculator();
            var statCalculator = new StatCalculator(this.buildingsRepository.GetBuildings(), false);

            var monsters = this.monsterQueries.GetMonsters();

            foreach (var monster in monsters)
            {
                var item = new MonsterListItem();
                item.InjectFrom(monster);
                item.TotalStrength = calculator.Calculate(monster.Runes.ToList());
                item.CritMultiplier = statCalculator.GetCritMultiplier(monster);
                item.PendingCritMultiplier = monster.PendingRunes.Any() ? statCalculator.GetCritMultiplier(monster, monster.PendingRunes.ToList()): new double?();
                item.HitMultiplier = statCalculator.GetHitMultiplier(monster,  monster.Runes);
                item.PendingHitMultiplier = monster.PendingRunes.Any() ? statCalculator.GetHitMultiplier(monster, monster.PendingRunes.ToList()) : new double?();
                item.DOTMultiplier = statCalculator.GetDOTMultiplier(monster, monster.Runes);
                item.PendingDOTMultiplier = monster.PendingRunes.Any() ? statCalculator.GetDOTMultiplier(monster, monster.PendingRunes.ToList()) : new double?();
                item.EHP = statCalculator.GetEffectiveHealth(monster, monster.Runes);
                item.PendingEHP = monster.PendingRunes.Any() ? statCalculator.GetEffectiveHealth(monster, monster.PendingRunes.ToList()) : new double?();
                item.Monster = monster;
                List.Add(item);
            }
            list = list.OrderByDescending(p => p.TotalStrength).ToList();

            NotifyOfPropertyChange(() => List);
        }
    }
}
