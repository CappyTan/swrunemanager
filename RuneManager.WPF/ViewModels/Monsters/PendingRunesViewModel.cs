﻿namespace RuneManager.WPF.ViewModels.Monsters
{
    using System.Collections.Generic;
    using System.Linq;

    using Caliburn.Micro;

    using Omu.ValueInjecter;

    using RuneManager.Domain.Base;
    using RuneManager.Domain.Contracts.Repositories;
    using RuneManager.Domain.Enumerations;
    using RuneManager.Domain.Extensions;
    using RuneManager.Domain.Helpers;
    using RuneManager.Domain.Models;
    using RuneManager.Services.Calculators;
    using RuneManager.WPF.Queries.Buildings;
    using RuneManager.WPF.Queries.Monsters;

    public class PendingRunesViewModel : Screen
    {
        private readonly IMonsterQueries monsterQueries;

        private readonly IMonsterRepository monsterRepository;

        private readonly IBuildingsQueries buildingsQueries;

        private IList<MonsterListItem> list;

        public PendingRunesViewModel(IMonsterQueries monsterQueries, IMonsterRepository monsterRepository, IBuildingsQueries buildingsQueries)
        {
            this.monsterQueries = monsterQueries;
            this.monsterRepository = monsterRepository;
            this.buildingsQueries = buildingsQueries;
        }

        public IList<MonsterListItem> List
        {
            get
            {
                return this.list;
            }
        }

        public void ClearPendingRunes(MonsterListItem item)
        {
            var monster = this.monsterRepository.Find(item.Id);
            monster.PendingRunes = new List<Rune>();
            this.monsterRepository.Update(monster);

            this.SetList();
        }

        public void CommitPendingRunes(MonsterListItem item)
        {
            var monster = this.monsterRepository.Find(item.Id);
            monster.Runes = monster.PendingRunes;
            monster.PendingRunes = new List<Rune>();
            this.monsterRepository.Update(monster);

            this.SetList();
        }

        protected override void OnActivate()
        {
            base.OnActivate();
            
            this.SetList();
        }

        private void SetList()
        {
            var calculator = new TotalStatsCalculator();
            var critMultiplier = new StatCalculator(buildingsQueries.GetBuildings(), false);
            list  = new List<MonsterListItem>();
            

            var monsters = this.monsterQueries.GetMonsters();

            foreach (var monster in monsters.Where(p => p.PendingRunes.Any()))
            {
                var item = new MonsterListItem();
                item.InjectFrom(monster);
                item.PendingRunes = monster.PendingRunes.OrderBy(p => p.Slot).ToList();
                item.TotalStrength = calculator.Calculate(monster.Runes.ToList());
                item.CritMultiplier = critMultiplier.GetCritMultiplier(monster);
                item.HP = monster.TotalHitPoints(monster.PendingRunes);
                item.PendingCritMultiplier = critMultiplier.GetCritMultiplier(monster, monster.PendingRunes.ToList());
                item.TotalSpeed = monster.PendingRunes.TotalStats(StatType.SPD) +  SetBonusHelper.GetSwiftBonus(monster, monster.PendingRunes) + monster.Speed ?? monster.Speed;
                this.List.Add(item);
            }
            this.list = this.list.OrderByDescending(p => p.TotalStrength).ToList();

            this.NotifyOfPropertyChange(() => this.List);
        }
    }
}
