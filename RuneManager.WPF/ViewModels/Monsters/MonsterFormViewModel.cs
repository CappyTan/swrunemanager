﻿namespace RuneManager.WPF.ViewModels.Monsters
{
    using System.Collections.Generic;
    using System.Linq;

    using Caliburn.Micro;

    using Omu.ValueInjecter;

    using RuneManager.Domain.Contracts.Repositories;
    using RuneManager.Domain.Contracts.Services;
    using RuneManager.Domain.Enumerations;
    using RuneManager.Domain.Helpers;
    using RuneManager.Domain.Models;
    using RuneManager.WPF.Code;

    public class MonsterFormViewModel : Screen
    {
        private readonly IMonsterService monsterService;

        private readonly IMonsterRepository monsterRepository;

        private readonly int? id;

        private Monster monster;

        private readonly IList<ComboBoxItem<Element>> elementList = ComboBoxHelpers.GetElementList();

        public MonsterFormViewModel(IMonsterService monsterService, IMonsterRepository monsterRepository, int? id)
        {
            this.monsterService = monsterService;
            this.monsterRepository = monsterRepository;
            this.id = id;
            this.DisplayName = "Monster Form";
        }

        public string ButtonText
        {
            get
            {
                if (id.HasValue)
                {
                    return "Edit Monster";
                }

                return "Add Monster";
            }
        }

        public string Name { get; set; }

        public ComboBoxItem<Element> Element { get; set; }

        public IList<ComboBoxItem<Element>> ElementList
        {
            get
            {
                return this.elementList;
            }
        }

        public int HitPoints { get; set; }

        public int Defense { get; set; }

        public int Attack { get; set; }

        public int Speed { get; set; }

        public int CritChance { get; set; }

        public int CritDamage { get; set; }

        public int Resistance { get; set; }

        public int Accuracy { get; set; }

        public string SuccessMessage { get; set; }

        public void Process()
        {
            if (!id.HasValue)
            {
                monster = new Monster();
            }
            
            monster.InjectFrom(this);
            monster.Element = this.Element.Type;


            if (!id.HasValue)
            {
                this.monsterService.AddMonster(monster);
                this.ClearForm();
            }
            else
            {
                this.monsterService.UpdateMonster(monster);
                this.SuccessMessage = "Monster Updated";
                this.NotifyOfPropertyChange(() => SuccessMessage);
            }
            
            

        }

        protected override void OnActivate()
        {
            base.OnActivate();

            if (id.HasValue)
            {
                monster = this.monsterRepository.Find(id.Value);
                this.InjectFrom(monster);
                this.Element = this.elementList.FirstOrDefault(p => p.Type == monster.Element);
                this.Refresh();
            }

            

        }

        private void ClearForm()
        {
            this.Name = null;
            this.Element = null;
            this.HitPoints = 0;
            this.Defense = 0;
            this.Attack = 0;
            this.Speed = 0;
            this.CritChance = 0;
            this.CritDamage = 0;
            this.Accuracy = 0;
            this.Resistance = 0;

            this.SuccessMessage = "Monster Added";

            this.Refresh();
        }
    }
}
