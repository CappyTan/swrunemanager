﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RuneManager.WPF.ViewModels.Tools
{
    using System.ComponentModel;

    using Caliburn.Micro;

    using RuneManager.Domain.Base;
    using RuneManager.Domain.Contracts.Repositories;
    using RuneManager.Domain.Enumerations;
    using RuneManager.Domain.Helpers;
    using RuneManager.Domain.Models;
    using RuneManager.Domain.Models.ValueObjects;
    using RuneManager.Services.Calculators;
    using RuneManager.Services.Optimizers;
    using RuneManager.WPF.Code;
    using RuneManager.WPF.Queries.Monsters;
    using RuneManager.WPF.Queries.Runes;

    using WellPoint.Marketing.CampaignTracker.Framework.Extensions;

    public class OptimizerViewModel : Screen
    {
        private readonly IMonsterQueries monsterQueries;

        private readonly IMonsterRepository monsterRepository;

        private readonly IRuneRepository runeRepository;

        private readonly OptimizerBase optimizer;

        private readonly ScalingType scalingType;

        private readonly IList<ComboBoxItem<SetType>> fourSetTypeList = ComboBoxHelpers.GetFourSetList();

        private readonly IList<ComboBoxItem<SetType>> twoSetTypeList = ComboBoxHelpers.GetTwoSetList();

        private readonly IList<ComboBoxItem<SetType>> twoSetTypeList2 = ComboBoxHelpers.GetTwoSetList();

        private readonly IList<ComboBoxItem<SetType>> twoSetTypeList3 = ComboBoxHelpers.GetTwoSetList();

        private BackgroundWorker worker;

        public OptimizerViewModel(IMonsterQueries monsterQueries, IMonsterRepository monsterRepository, IRuneRepository runeRepository, OptimizerBase optimizer, ScalingType scalingType)
        {
            this.monsterQueries = monsterQueries;
            this.monsterRepository = monsterRepository;
            this.runeRepository = runeRepository;
            this.optimizer = optimizer;
            this.scalingType = scalingType;

            this.ShowTwoSetType2 = true;
            this.ShowTwoSetType3 = true;
        }

        public Monster Monster { get; set; }
        public IList<Monster> Monsters { get; set; }

        public IList<Item> Items { get; set; }

        protected override void OnViewLoaded(object view)
        {
            base.OnViewLoaded(view);
            this.Monsters = this.monsterQueries.GetMonsters().OrderBy(p => p.Name).ToList();
            this.NotifyOfPropertyChange(() => Monsters);
        }

        private ComboBoxItem<SetType> fourSetType;

        public ComboBoxItem<SetType> FourSetType
        {
            get
            {
                return fourSetType;
            }
            set
            {
                fourSetType = value;
                this.ShowTwoSetType2 = false;
                this.ShowTwoSetType3 = false;
                this.NotifyOfPropertyChange(() => FourSetType);
                this.NotifyOfPropertyChange(() => ShowTwoSetType2);
                this.NotifyOfPropertyChange(() => ShowTwoSetType3);
            }
        }

        public IList<ComboBoxItem<SetType>> FourSetTypeList
        {
            get
            {
                return this.fourSetTypeList;
            }
        }

        public IList<OptimizerSpeedType> OptimizerSpeedList
        {
            get
            {
                return EnumerationExtensions.EnumToList<OptimizerSpeedType>();
            }
        }

        public OptimizerSpeedType OptimizerSpeed { get; set; }

        public ComboBoxItem<SetType> TwoSetType { get; set; }

        public ComboBoxItem<SetType> TwoSetType2 { get; set; }

        public ComboBoxItem<SetType> TwoSetType3 { get; set; }

        public IList<ComboBoxItem<SetType>> TwoSetTypeList
        {
            get
            {
                return this.twoSetTypeList;
            }
        }

        public IList<ComboBoxItem<SetType>> TwoSetTypeList2
        {
            get
            {
                return this.twoSetTypeList2;
            }
        }

        public IList<ComboBoxItem<SetType>> TwoSetTypeList3
        {
            get
            {
                return this.twoSetTypeList3;
            }
        }

        public bool ShowTwoSetType2 { get; set; }
        public bool ShowTwoSetType3 { get; set; }

        public string Description
        {
            get
            {
                return optimizer.Description;
            }
        }

        public string Examples
        {
            get
            {
                return string.Format("Exmaples: {0}", optimizer.Examples);
            }
        }

        public int? Speed { get; set; }

        public int? MaxSpeed { get; set; }

        public int? CritChance { get; set; }

        public int? MaxCritChance { get; set; }

        public int? Accuracy { get; set; }

        public int? Resistance { get; set; }

        public int? HitPoints { get; set; }

        public int? Defense { get; set; }

        public int? EffectiveHitPoints { get; set; }

        public long CurrentCombination { get; set; }

        public long TotalCombinations { get; set; }

        public int SkillUp { get; set; }

        public bool IsGuildWars { get; set; }

        public bool IgnoreEquipped { get; set; }

        public void SelectRuneSet(Item item)
        {
            Monster.PendingRunes = item.Runes;
            monsterRepository.Update(Monster);
        }

        public void Calculate()
        {   
            worker = new BackgroundWorker();
            worker.DoWork += this.DoWork;
            worker.RunWorkerCompleted += this.WorkerCompleted;
            worker.ProgressChanged += this.ProgressChanged;
            worker.WorkerReportsProgress = true;

            worker.RunWorkerAsync();
        }

        void WorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            this.NotifyOfPropertyChange(() => Items);
        }

        void ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            OptimizerProgress progress = e.UserState as OptimizerProgress;

            CurrentCombination = progress.CurrentCombination;
            TotalCombinations = progress.TotalCombinations;

            this.NotifyOfPropertyChange(() => CurrentCombination);
            this.NotifyOfPropertyChange(() => TotalCombinations);
        }

        public void DoWork(object sender, DoWorkEventArgs e)
        {
            var runes = this.runeRepository.GetRunes();

            var parameters = new OptimizerParameters
                                 {
                                     CritBuff = true,
                                     FourSet = FourSetType != null ? FourSetType.Type : new SetType?(),
                                     TwoSet = TwoSetType != null ? TwoSetType.Type : new SetType?(),
                                     TwoSet2 = TwoSetType2 != null ? TwoSetType2.Type : new SetType?(),
                                     TwoSet3 = TwoSetType3 != null ? TwoSetType3.Type : new SetType?(),
                                     MinCritChance = CritChance ?? Monster.CritChance,
                                     MaxCritChance = MaxCritChance ??  Int32.MaxValue,
                                     MinSpeed = Speed ?? Monster.Speed,
                                     MaxSpeed = MaxSpeed ?? Int32.MaxValue,
                                     MinAccuracy = Accuracy ?? Monster.Accuracy,
                                     MinResistance = Resistance ?? Monster.Resistance,
                                     MinHitPoints = HitPoints ?? Monster.HitPoints,
                                     MinDefense = Defense ?? Monster.Defense,
                                     MinEffectiveHitPoints = EffectiveHitPoints ?? 0,
                                     ScalingType = scalingType,
                                     SkillUp = SkillUp,
                                     IsGuildWars = IsGuildWars,
                                     IgnoreEquipped = IgnoreEquipped,
                                     OptimizerSpeed = this.OptimizerSpeed
                                 };

            var runeSets = this.optimizer.FindSets(Monster, runes, parameters, worker);
                
            var items = new List<Item>();

            foreach (var runeSet in runeSets)
            {
                Item item = new Item();
                item.Runes.AddRange(runeSet.Runes);
                item.CritChance = runeSet.TotalCrit + Monster.CritChance;
                item.HitMultiplier = runeSet.HitMultiplier;
                item.CritMultiplier = runeSet.CritMultiplier;
                item.Attack = Monster.TotalAttack(runeSet.Runes);
                item.CritDamage = Monster.TotalCritDamage(runeSet.Runes);
                item.Speed = runeSet.TotalSpeed + Monster.Speed;
                item.Accuracy = runeSet.TotalAccuracy + Monster.Accuracy;
                item.HitPoints = runeSet.TotalHitPoints;
                item.Defense = runeSet.TotalDefense;
                item.Resistance = runeSet.TotalResistance + Monster.Resistance;
                item.EffectiveHealth = runeSet.EffectiveHealth;
                item.EffectiveRaidTankHealth = runeSet.EffectiveRaidFrontLineHealth;
                item.DOTMultiplier = runeSet.DOTMultiplier;

                items.Add(item);
            }

            Items = items;
        }
    }

    public class Item
    {
        public Item()
        {
            this.Runes = new List<Rune>();
        }

        public List<Rune> Runes { get; set; }

        public double CritChance { get; set; }

        public double CritMultiplier { get; set; }

        public double DOTMultiplier { get; set; }

        public double EffectiveHealth { get; set; }

        public double Attack { get; set; }

        public int CritDamage { get; set; }

        public double Speed { get; set; }

        public int Accuracy { get; set; }

        public double Defense { get; set; }

        public double HitPoints { get; set; }

        public int Resistance { get; set; }

        public double HitMultiplier { get; set; }

        public double EffectiveRaidTankHealth { get; set; }
    }
}
