﻿namespace RuneManager.WPF.ViewModels.Options
{
    using System.Collections.Generic;
    using System.Linq;

    using Caliburn.Micro;

    using RuneManager.Domain.Contracts.Services;
    using RuneManager.Domain.Enumerations;
    using RuneManager.Domain.Helpers;
    using RuneManager.WPF.Code;
    using RuneManager.WPF.Queries.Runes;

    public class ImportRunesFormViewModel : Screen
    {
        private readonly IRuneService runeService;

        private readonly IRuneQueries runeQueries;

        private readonly IList<ComboBoxItem<SetType>> twoSetTypeList = ComboBoxHelpers.GetTwoSetList();

        public ImportRunesFormViewModel(IRuneService runeService, IRuneQueries runeQueries)
        {
            this.runeService = runeService;
            this.runeQueries = runeQueries;
            this.NotifyOfPropertyChange(() => this.TwoSetTypeList);
        }

        public IList<ComboBoxItem<SetType>> TwoSetTypeList
        {
            get
            {
                return this.twoSetTypeList;
            }
        }

        public bool DeleteExistingRunes { get; set; }

        public string JSON { get; set; }

        public string SuccessMessage { get; set; }

        public bool UseMaxLevelMainStats { get; set; }

        public IList<RuneListItem> Errors { get; set; }


        public void Process()
        {
            var errors = this.runeService.ImportRunes(this.JSON, this.DeleteExistingRunes, UseMaxLevelMainStats);
            if (errors.Any())
            {
                this.SuccessMessage = "Your Runes have successfully Imported with errors. See below!";
                var runelistItems = this.runeQueries.GetRuneList(errors);
                this.Errors = runelistItems;
                this.NotifyOfPropertyChange(() => this.Errors);
            }
            else
            {
                this.SuccessMessage = "Your Runes have successfully Imported!";
            }
            
            this.NotifyOfPropertyChange(() => this.SuccessMessage);
        }
    }
}
