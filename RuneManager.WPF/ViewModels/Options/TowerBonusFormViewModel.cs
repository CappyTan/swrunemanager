﻿namespace RuneManager.WPF.ViewModels.Options
{
    using Caliburn.Micro;

    using Omu.ValueInjecter;

    using RuneManager.Domain.Contracts.Services;
    using RuneManager.Domain.Models;
    using RuneManager.WPF.Queries.Buildings;

    public class TowerBonusFormViewModel : Screen
    {
        private readonly IBuildingsQueries buildingsQueries;

        private readonly IBuildingsService buildingsService;

        private Buildings buildings;

        public TowerBonusFormViewModel(IBuildingsQueries buildingsQueries, IBuildingsService buildingsService)
        {
            this.buildingsQueries = buildingsQueries;
            this.buildingsService = buildingsService;
        }

        public int DefenseTower { get; set; }
        public int AttackTower { get; set; }
        public int CritDamageTower { get; set; }
        public int HitPointTower { get; set; }
        public int WindTower { get; set; }
        public int FireTower { get; set; }
        public int WaterTower { get; set; }
        public int DarkTower { get; set; }
        public int LightTower { get; set; }

        public int SpeedTower { get; set; }

        public int DefenseFlag { get; set; }
        public int AttackFlag { get; set; }
        public int CritDamageFlag { get; set; }
        public int HitPointFlag { get; set; }

        public string SuccessMessage { get; set; }

        public void Process()
        {
            this.buildings.InjectFrom(this);

            this.buildingsService.Update(buildings);

            this.SuccessMessage = "Buildings Updated";

            this.NotifyOfPropertyChange(() => SuccessMessage);
        }

        protected override void OnActivate()
        {
            base.OnActivate();

            buildings = this.buildingsQueries.GetBuildings();
            this.InjectFrom(buildings);
            this.Refresh();
        }
    }
}
