﻿namespace RuneManager.WPF.ViewModels.Runes
{
    using System.Collections.Generic;

    using Caliburn.Micro;

    using RuneManager.WPF.Messages;
    using RuneManager.WPF.Queries.Runes;

    public class RuneListViewModel : Screen
    {
        private readonly IRuneQueries runeQueries;

        protected readonly IEventAggregator eventAggregator;

        private IList<RuneListItem> list;

        public RuneListViewModel(IRuneQueries runeQueries, IEventAggregator eventAggregator)
        {
            this.runeQueries = runeQueries;
            this.eventAggregator = eventAggregator;
        }

        public IList<RuneListItem> List
        {
            get
            {
                return this.list;
            }
        }

        public void EditRune(RuneListItem item)
        {
            this.eventAggregator.PublishOnUIThread(new FormViewChangeMessage(item.Id, "RuneForm"));
        }

        protected override void OnActivate()
        {
            base.OnActivate();

            list = this.runeQueries.GetRuneList();
            NotifyOfPropertyChange(() => List);
        }
    }
}
