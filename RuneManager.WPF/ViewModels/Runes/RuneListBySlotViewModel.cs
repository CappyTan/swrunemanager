﻿namespace RuneManager.WPF.ViewModels.Runes
{
    using System.Collections.Generic;
    using System.Linq;

    using Caliburn.Micro;

    using RuneManager.Domain.Contracts.Services;
    using RuneManager.Domain.Enumerations;
    using RuneManager.Domain.Helpers;
    using RuneManager.WPF.Code;
    using RuneManager.WPF.Messages;
    using RuneManager.WPF.Queries.Runes;
    using RuneManager.WPF.ViewModels.Layout;

    public class RuneListBySlotViewModel : Screen
    {
        private readonly IRuneQueries runeQueries;

        private readonly IRuneService runeService;

        private readonly IEventAggregator eventAggregator;

        private readonly IWindowManager windowManager;

        private readonly IList<CheckBoxItem<SetType>> setTypeList = CheckBoxHelpers.GetRuneSetList();

        private readonly IList<CheckBoxItem<StatType>> statTypeList = CheckBoxHelpers.GetStatTypeList();

        private IList<RuneListItem> fullList;
        private List<RuneListItem> filteredList = new List<RuneListItem>(); 

        private IList<RuneListItem> slot1;
        private IList<RuneListItem> slot2;
        private IList<RuneListItem> slot3;
        private IList<RuneListItem> slot4;
        private IList<RuneListItem> slot5;
        private IList<RuneListItem> slot6;

        public RuneListBySlotViewModel(IRuneQueries runeQueries, IRuneService runeService, IEventAggregator eventAggregator, IWindowManager windowManager)
        {
            this.runeQueries = runeQueries;
            this.runeService = runeService;
            this.eventAggregator = eventAggregator;
            this.windowManager = windowManager;
        }

        public IList<RuneListItem> Slot1
        {
            get
            {
                return this.slot1;
            }
        }

        public IList<RuneListItem> Slot2
        {
            get
            {
                return this.slot2;
            }
        }

        public IList<RuneListItem> Slot3
        {
            get
            {
                return this.slot3;
            }
        }

        public IList<RuneListItem> Slot4
        {
            get
            {
                return this.slot4;
            }
        }

        public IList<RuneListItem> Slot5
        {
            get
            {
                return this.slot5;
            }
        }

        public IList<RuneListItem> Slot6
        {
            get
            {
                return this.slot6;
            }
        }

        public IList<CheckBoxItem<SetType>> SetTypeList
        {
            get
            {
                return this.setTypeList;
            }
        }

        public IList<CheckBoxItem<StatType>> StatTypeList
        {
            get
            {
                return this.statTypeList;
            }
        }

        public decimal? HPWeight { get; set; }
        public decimal? DEFWeight { get; set; }
        public decimal? ATKWeight { get; set; }
        public decimal? CRWeight { get; set; }
        public decimal? CDWeight { get; set; }
        public decimal? SPDWeight { get; set; }
        public decimal? ACCWeight { get; set; }
        public decimal? RESWeight { get; set; }

        public void EditRune(RuneListItem item)
        {
            this.eventAggregator.PublishOnUIThread(new FormViewChangeMessage(item.Id, "RuneForm"));
        }

        public void SellRune(RuneListItem item)
        {
            var model = new ConfirmationViewModel("Are you sure you wish to sell this rune?");
            windowManager.ShowDialog(model);

            if (model.Confirmed)
            {
                runeService.SellRune(item.Rune);
                this.filteredList.Remove(item);
                this.PopulateRunes(filteredList);
            }
        }

        public void FilterRunes()
        {
            filteredList.Clear();
            if (SetTypeList.All(p => !p.IsChecked))
            {
                filteredList.AddRange(fullList);
            }
            else
            {
                foreach (var set in SetTypeList)
                {
                    if (set.IsChecked)
                    {
                        filteredList.AddRange(fullList.Where(p => p.Set == set.Type));
                    }
                }
            }

            if (!StatTypeList.All(p => !p.IsChecked))
            {
                foreach (var stat in StatTypeList)
                {
                    if (!stat.IsChecked)
                    {
                        filteredList = filteredList.Where(p => p.Rune.MainStat.Type != stat.Type).ToList();
                    }
                }
            }
            

            filteredList = filteredList.OrderByDescending(p => p.TotalStats).ToList();

            this.runeQueries.UpdateWeights(filteredList, HPWeight, DEFWeight, ATKWeight, RESWeight, ACCWeight, SPDWeight, CRWeight, CDWeight);

            PopulateRunes(filteredList.OrderByDescending(p => p.TotalStats).ToList());
        }

        
        protected override void OnViewLoaded(object view)
        {
            base.OnViewLoaded(view);

            fullList = this.runeQueries.GetRuneList();
            this.filteredList.Clear();
            this.filteredList.AddRange(fullList);

            PopulateRunes(fullList);
        }

        private void PopulateRunes(IList<RuneListItem> list)
        {
            

            slot1 = list.Where(p => p.Slot == 1).ToList();
            slot2 = list.Where(p => p.Slot == 2).ToList();
            slot3 = list.Where(p => p.Slot == 3).ToList();
            slot4 = list.Where(p => p.Slot == 4).ToList();
            slot5 = list.Where(p => p.Slot == 5).ToList();
            slot6 = list.Where(p => p.Slot == 6).ToList();

            NotifyOfPropertyChange(() => Slot1);
            NotifyOfPropertyChange(() => Slot2);
            NotifyOfPropertyChange(() => Slot3);
            NotifyOfPropertyChange(() => Slot4);
            NotifyOfPropertyChange(() => Slot5);
            NotifyOfPropertyChange(() => Slot6);
        }
    }
}
