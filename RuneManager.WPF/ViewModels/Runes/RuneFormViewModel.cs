﻿namespace RuneManager.WPF.ViewModels.Runes
{
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;
    using System.Windows.Controls;

    using Caliburn.Micro;

    using Omu.ValueInjecter;

    using RuneManager.Domain;
    using RuneManager.Domain.Contracts.Services;
    using RuneManager.Domain.Enumerations;
    using RuneManager.Domain.Helpers;
    using RuneManager.Domain.Models;
    using RuneManager.WPF.Code;
    using RuneManager.WPF.Queries.Monsters;

    public class RuneFormViewModel : Screen
    {
        private readonly IRuneService runeService;

        private readonly IMonsterQueries monsterQueries;

        private readonly int? id;

        private readonly IList<ComboBoxItem<SetType>> setTypeList = ComboBoxHelpers.GetRuneSetList();

        private readonly IList<int?> slotList = ComboBoxHelpers.GetSlotList();

        private readonly IList<ComboBoxItem<StatType>> mainStatList = ComboBoxHelpers.GetStatTypeList();

        private readonly IList<ComboBoxItem<StatType>> bonusStatList = ComboBoxHelpers.GetStatTypeList();

        private readonly IList<ComboBoxItem<StatType>> stat1List = ComboBoxHelpers.GetStatTypeList();

        private readonly IList<ComboBoxItem<StatType>> stat2List = ComboBoxHelpers.GetStatTypeList();

        private readonly IList<ComboBoxItem<StatType>> stat3List = ComboBoxHelpers.GetStatTypeList();

        private readonly IList<ComboBoxItem<StatType>> stat4List = ComboBoxHelpers.GetStatTypeList();

        private Rune rune;


        public RuneFormViewModel(IRuneService runeService, IMonsterQueries monsterQueries, int? id)
        {
            this.runeService = runeService;
            this.monsterQueries = monsterQueries;
            this.id = id;
            this.DisplayName = "Rune Form";
        }

        public string ButtonText
        {
            get
            {
                if (id.HasValue)
                {
                    return "Edit Rune";
                }

                return "Add Rune";
            }
        }

        public ComboBoxItem<SetType> SetType { get; set; }

        public IList<ComboBoxItem<SetType>> SetTypeList
        {
            get
            {
                return this.setTypeList;
            }
        }

        private int? slot;

        public int? Slot
        {
            get
            {
                return slot;
            }
            set
            {
                slot = value;
                this.NotifyOfPropertyChange(() => Slot);
                if (slot == 1)
                {
                    this.MainStatType = MainStatList.Single(p => p.Type == StatType.ATK);
                    this.MainStatValue = 160;
                }

                else if (slot == 3)
                {
                    this.MainStatType = MainStatList.Single(p => p.Type == StatType.DEF);
                    this.MainStatValue = 160;
                }

                else if (slot == 5)
                {
                    this.MainStatType = MainStatList.Single(p => p.Type == StatType.HP);
                    this.MainStatValue = 2448;
                }
                else
                {
                    this.MainStatType = null;
                    this.MainStatValue = null;
                }


                this.NotifyOfPropertyChange(() => MainStatType);
                this.NotifyOfPropertyChange(() => MainStatValue);
            }
        }

        public IList<int?> SlotList
        {
            get
            {
                return slotList;
            }
        }

        private ComboBoxItem<StatType> mainStatType;

        public ComboBoxItem<StatType> MainStatType
        {
            get
            {
                return mainStatType;
            }
            set
            {
                mainStatType = value;
                this.NotifyOfPropertyChange(() => MainStatType);
                if (MainStatType != null)
                {
                    int? newValue;
                    switch (MainStatType.Type)
                    {
                        case StatType.ACC:
                        case StatType.RES:
                            newValue = 64;
                            break;
                        case StatType.DEFPercentage:
                        case StatType.ATKPercentage:
                        case StatType.HPPercentage:
                            newValue = 63;
                            break;
                        case StatType.CD:
                            newValue = 80;
                            break;
                        case StatType.CR:
                            newValue = 58;
                            break;
                        case StatType.SPD:
                            newValue = 42;
                            break;
                        default:
                            newValue = null;
                            break;
                    }

                    MainStatValue = newValue;
                    this.NotifyOfPropertyChange(() => MainStatValue);
                }
            }
        }

        public IList<ComboBoxItem<StatType>> MainStatList
        {
            get
            {
                return this.mainStatList;
            }
        }

        public int? MainStatValue { get; set; }

        public ComboBoxItem<StatType> BonusStatType { get; set; }

        public IList<ComboBoxItem<StatType>> BonusStatList
        {
            get
            {
                return this.bonusStatList;
            }
        }

        public int? BonusStatValue { get; set; }

        public ComboBoxItem<StatType> Stat1Type { get; set; }

        public IList<ComboBoxItem<StatType>> Stat1List
        {
            get
            {
                return this.stat1List;
            }
        }

        public int? Stat1Value { get; set; }

        public ComboBoxItem<StatType> Stat2Type { get; set; }

        public IList<ComboBoxItem<StatType>> Stat2List
        {
            get
            {
                return this.stat2List;
            }
        }

        public int? Stat2Value { get; set; }

        public bool Locked { get; set; }

        public ComboBoxItem<StatType> Stat3Type { get; set; }

        public IList<ComboBoxItem<StatType>> Stat3List
        {
            get
            {
                return this.stat3List;
            }
        }

        public int? Stat3Value { get; set; }

        public ComboBoxItem<StatType> Stat4Type { get; set; }

        public IList<ComboBoxItem<StatType>> Stat4List
        {
            get
            {
                return this.stat4List;
            }
        }

        public int? Stat4Value { get; set; }

        public bool ShowSetTypeError { get; set; }
        public bool ShowSlotError { get; set; }

        public string SuccessMessage { get; set; }

        public ComboBoxItem<Monster> Monster { get; set; }
        public IList<ComboBoxItem<Monster>> Monsters { get; set; }

        public ComboBoxItem<Monster> PendingMonster { get; set; }
        public IList<ComboBoxItem<Monster>> PendingMonsters { get; set; }

        protected override void OnActivate()
        {
            base.OnActivate();
            this.Monsters = this.monsterQueries.GetMonsters().OrderBy(p => p.Name).Select(p => new ComboBoxItem<Monster> { Type = p, Label = p.Name }).ToList();
            this.Monsters.Insert(0, new ComboBoxItem<Monster> { Type = null, Label = "None" });

            this.PendingMonsters = this.monsterQueries.GetMonsters().OrderBy(p => p.Name).Select(p => new ComboBoxItem<Monster> { Type = p, Label = p.Name }).ToList();
            this.PendingMonsters.Insert(0, new ComboBoxItem<Monster> { Type = null, Label = "None" });

            if (id.HasValue)
            {
                this.rune = this.runeService.GetRune(id.Value);
                this.Monster = this.Monsters.FirstOrDefault(p => rune.Monster != null && p.Type != null && p.Type.Id == rune.Monster.Id);
                this.PendingMonster = this.PendingMonsters.FirstOrDefault(p => rune.PendingMonster != null && p.Type != null && p.Type.Id == rune.PendingMonster.Id);
                this.Slot = rune.Slot;
                this.SetType = this.setTypeList.FirstOrDefault(p => p.Type == rune.Type);
                this.MainStatType = this.mainStatList.FirstOrDefault(p => p.Type == rune.MainStat.Type);
                this.BonusStatType = this.bonusStatList.FirstOrDefault(p => p.Type == rune.BonusStat.Type);
                this.Stat1Type = this.stat1List.FirstOrDefault(p => p.Type == rune.Stat1.Type);
                this.Stat2Type = this.stat2List.FirstOrDefault(p => p.Type == rune.Stat2.Type);
                this.Stat3Type = this.stat3List.FirstOrDefault(p => p.Type == rune.Stat3.Type);
                this.Stat4Type = this.stat4List.FirstOrDefault(p => p.Type == rune.Stat4.Type);

                this.MainStatValue = rune.MainStat.Value;
                this.BonusStatValue = rune.BonusStat.Value;
                this.Stat1Value = rune.Stat1.Value;
                this.Stat2Value = rune.Stat2.Value;
                this.Stat3Value = rune.Stat3.Value;
                this.Stat4Value = rune.Stat4.Value;

                this.Locked = rune.Locked;
            }



            this.NotifyOfPropertyChange(() => Monsters);
        }

        public void Process()
        {
            if (!id.HasValue)
            {
                rune = new Rune();
            }
            
            rune.MainStat = new Stat { Type = MainStatType.Type, Value = MainStatValue };
            if (this.BonusStatType != null)
            {
                rune.BonusStat = new Stat { Type = this.BonusStatType.Type, Value = this.BonusStatValue };
            }

            if (this.Stat1Type != null)
            {
                rune.Stat1 = new Stat { Type = Stat1Type.Type, Value = Stat1Value };
            }

            if (this.Stat2Type != null)
            {
                rune.Stat2 = new Stat { Type = Stat2Type.Type, Value = Stat2Value };
            }

            if (this.Stat3Type != null)
            {
                rune.Stat3 = new Stat { Type = Stat3Type.Type, Value = Stat3Value };
            }

            if (this.Stat4Type != null)
            {
                rune.Stat4 = new Stat { Type = Stat4Type.Type, Value = Stat4Value };
            }

            rune.Type = SetType.Type;
            rune.Slot = Slot.Value;
            rune.Monster = Monster != null ? Monster.Type : null;
            rune.PendingMonster = PendingMonster != null ? PendingMonster.Type : null;
            rune.Locked = Locked;

            if (!id.HasValue)
            {
                this.runeService.AddRune(rune);
                this.ClearForm();
            }
            else
            {
                this.runeService.UpdateRune(rune);
                this.SuccessMessage = "Rune Updated";
                this.NotifyOfPropertyChange(() => SuccessMessage);
            }
            

        }

        private void ClearForm()
        {
            this.SetType = null;
            this.Slot = null;
            this.MainStatType = null;
            this.MainStatValue = null;
            this.BonusStatType = null;
            this.BonusStatValue = null;
            this.Stat1Type = null;
            this.Stat1Value = null;
            this.Stat2Type = null;
            this.Stat2Value = null;
            this.Stat3Type = null;
            this.Stat3Value = null;
            this.Stat4Type = null;
            this.Stat4Value = null;
            this.Monster = null;
            this.Locked = false;

            this.SuccessMessage = "Rune Added";

            this.Refresh();
        }
    }
}
