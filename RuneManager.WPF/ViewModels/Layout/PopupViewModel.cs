﻿namespace RuneManager.WPF.ViewModels.Layout
{
    public class PopupViewModel 
    {
        public string Title { get; set; }

        public PopupViewModel(string title)
        {
            this.Title = title;
        }
    }
}
