namespace RuneManager.WPF.ViewModels.Layout
{
    using System.IO;

    using Caliburn.Micro;

    using RuneManager.Domain.Contracts.Repositories;
    using RuneManager.Domain.Contracts.Services;
    using RuneManager.Domain.Enumerations;
    using RuneManager.Services.Optimizers;
    using RuneManager.Services.Optimizers.Attack;
    using RuneManager.Services.Optimizers.Defense;
    using RuneManager.Services.Optimizers.HitPoint;
    using RuneManager.WPF.Messages;
    using RuneManager.WPF.Queries.Buildings;
    using RuneManager.WPF.Queries.Monsters;
    using RuneManager.WPF.Queries.Runes;
    using RuneManager.WPF.ViewModels.Monsters;
    using RuneManager.WPF.ViewModels.Options;
    using RuneManager.WPF.ViewModels.Runes;
    using RuneManager.WPF.ViewModels.Tools;

    public class ShellViewModel : Conductor<object>, IShell, IHandle<FormViewChangeMessage>
    {
        

        private readonly IEventAggregator _eventAggregator;
        private readonly IWindowManager windowManager;

        private readonly IRuneService runeService;

        private readonly IRuneQueries runeQueries;

        private readonly IMonsterService monsterService;

        private readonly IMonsterQueries monsterQueries;

        private readonly IRuneRepository runeRepository;

        private readonly IMonsterRepository monsterRepository;

        private readonly IBuildingsQueries buildingsQueries;

        private readonly IBuildingsService buildingsService;

        private readonly IBuildingsRepository buildingsRepository;

        public ShellViewModel(IEventAggregator eventAggregator, IWindowManager windowManager, 
            IRuneService runeService, IRuneQueries runeQueries, IMonsterService monsterService, 
            IMonsterQueries monsterQueries, IRuneRepository runeRepository, IMonsterRepository monsterRepository,
            IBuildingsQueries buildingsQueries, IBuildingsService buildingsService, IBuildingsRepository buildingsRepository)
        {
            this.DisplayName = "SW Rune Manager";

            
            this._eventAggregator = eventAggregator;
            this.windowManager = windowManager;
            this.runeService = runeService;
            this.runeQueries = runeQueries;
            this.monsterService = monsterService;
            this.monsterQueries = monsterQueries;
            this.runeRepository = runeRepository;
            this.monsterRepository = monsterRepository;
            this.buildingsQueries = buildingsQueries;
            this.buildingsService = buildingsService;
            this.buildingsRepository = buildingsRepository;

            this._eventAggregator.Subscribe(this);
        }

        public void ShowAddRuneForm()
        {
            this.ShowRuneForm(new int?());
        }



        public void ShowRuneList()
        {
            this.ActivateItem(new RuneListViewModel(runeQueries, _eventAggregator));
        }

        public void ShowRunesBySlotList()
        {
            this.ActivateItem(new RuneListBySlotViewModel(runeQueries, runeService, _eventAggregator, windowManager));
        }

        public void ShowAddMonsterForm()
        {
            this.ShowMonsterForm(new int?());
        }

        public void ShowMonsterList()
        {
            this.ActivateItem(new MonsterListViewModel(monsterQueries, monsterRepository, buildingsRepository, _eventAggregator, windowManager));
        }

        public void ShowCritOptimizer()
        {
            this.ActivateItem(new OptimizerViewModel(monsterQueries, monsterRepository, runeRepository, new MaxCritOptimizer(buildingsRepository), ScalingType.Attack));
        }

        public void ShowHitOptimizer()
        {
            this.ActivateItem(new OptimizerViewModel(monsterQueries, monsterRepository, runeRepository, new MaxHitOptimizer(buildingsRepository), ScalingType.Attack));
        }

        public void ShowHPHitOptimizer()
        {
            this.ActivateItem(new OptimizerViewModel(monsterQueries, monsterRepository, runeRepository, new HitPointMaxHitOptimizer(buildingsRepository), ScalingType.HitPoints));
        }

        public void ShowDefHitOptimizer()
        {
            this.ActivateItem(new OptimizerViewModel(monsterQueries, monsterRepository, runeRepository, new DefenseMaxHitOptimizer(buildingsRepository), ScalingType.Defense));
        }

        public void ShowDOTOptimizer()
        {
            this.ActivateItem(new OptimizerViewModel(monsterQueries, monsterRepository, runeRepository, new DOTOptimizer(buildingsRepository), ScalingType.Attack));
        }

        public void ShowEnemyMaxHPOptimizer()
        {
            this.ActivateItem(new OptimizerViewModel(monsterQueries, monsterRepository, runeRepository, new EnemyMaxHPOptimizer(buildingsRepository), ScalingType.Attack));
        }

        public void ShowBomberOptimizer()
        {
            this.ActivateItem(new OptimizerViewModel(monsterQueries, monsterRepository, runeRepository, new BomberOptimizer(buildingsRepository), ScalingType.Attack));
        }

        public void ShowEHPOptimizer()
        {
            this.ActivateItem(new OptimizerViewModel(monsterQueries, monsterRepository, runeRepository, new EffectiveHealthOptimizer(buildingsRepository), ScalingType.Attack));
        }

        public void ShowSpeedOptimizer()
        {
            this.ActivateItem(new OptimizerViewModel(monsterQueries, monsterRepository, runeRepository, new SpeedOptimizer(buildingsRepository), ScalingType.Attack));
        }

        public void ShowATKBruiserOptimizer()
        {
            this.ActivateItem(new OptimizerViewModel(monsterQueries, monsterRepository, runeRepository, new AttackBruiserOptimizer(buildingsRepository), ScalingType.Attack));
        }

        public void ShowHPBruiserOptimizer()
        {
            this.ActivateItem(new OptimizerViewModel(monsterQueries, monsterRepository, runeRepository, new HitPointBruiserOptimizer(buildingsRepository), ScalingType.HitPoints));
        }

        public void ShowDefBruiserOptimizer()
        {
            this.ActivateItem(new OptimizerViewModel(monsterQueries, monsterRepository, runeRepository, new DefenseBruiserOptimizer(buildingsRepository), ScalingType.Defense));
        }

        public void ShowEnemyMaxHPBruiserOptimizer()
        {
            this.ActivateItem(new OptimizerViewModel(monsterQueries, monsterRepository, runeRepository, new EnemyMaxHPBruiserOptimizer(buildingsRepository), ScalingType.Attack));
        }

        public void ShowPendingRuneList()
        {
            this.ActivateItem(new PendingRunesViewModel(monsterQueries, monsterRepository, buildingsQueries));
        }

        public void ExportPendingRunes()
        {
            var filename = this.monsterService.ExportPendingRunes();
            var path = Path.GetFullPath(filename);
            System.Diagnostics.Process.Start(path);
        }

        public void SetTowerBonuses()
        {
            this.ActivateItem(new TowerBonusFormViewModel(buildingsQueries, buildingsService));
        }

        private void ShowRuneForm(int? id)
        {
            this.ActivateItem(new RuneFormViewModel(runeService, monsterQueries, id));
        }

        private void ShowMonsterForm(int? id)
        {
            this.ActivateItem(new MonsterFormViewModel(monsterService, monsterRepository, id));
        }

        public void Handle(FormViewChangeMessage message)
        {
            switch (message.Name)
            {
                case "RuneForm":
                    this.ShowRuneForm(message.Id);
                    break;
                case "MonsterForm":
                    this.ShowMonsterForm(message.Id);
                    break;
            }
        }
    }
}