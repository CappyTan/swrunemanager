﻿namespace RuneManager.WPF.ViewModels.Layout
{
    using Caliburn.Micro;

    public class ConfirmationViewModel : Screen
    {
        public string Message { get; set; }

        public bool Confirmed { get; set; }

        public ConfirmationViewModel(string message)
        {
            this.Message = message;
            this.DisplayName = "Are you sure?";
        }

        public void Yes()
        {
            this.Confirmed = true;
            this.TryClose();
        }

        public void No()
        {
            this.Confirmed = false;
            this.TryClose();
        }

    }
}
