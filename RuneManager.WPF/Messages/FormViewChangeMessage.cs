﻿namespace RuneManager.WPF.Messages
{
    public class FormViewChangeMessage
    {
        public FormViewChangeMessage(int? id, string name)
        {
            this.Id = id;
            this.Name = name;
        }

        public int? Id { get; private set; }

        public string Name { get; set; }
    }
}
