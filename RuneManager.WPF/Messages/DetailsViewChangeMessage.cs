﻿namespace RuneManager.WPF.Messages
{
    public class DetailsViewChangeMessage
    {
        public DetailsViewChangeMessage(int id, string name)
        {
            this.Id = id;
            this.Name = name;
        }

        public int Id { get; private set; }

        public string Name { get; set; }
    }
}
