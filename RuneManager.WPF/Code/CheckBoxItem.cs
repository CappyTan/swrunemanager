﻿namespace RuneManager.WPF.Code
{
    public class CheckBoxItem<T> : ComboBoxItem<T>
    {
        public bool IsChecked { get; set; }
    }
}
