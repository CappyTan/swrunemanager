﻿namespace RuneManager.WPF.Code
{
    public class ComboBoxItem<T>
    {
        public T Type { get; set; }

        public string Label { get; set; }
    }
}
