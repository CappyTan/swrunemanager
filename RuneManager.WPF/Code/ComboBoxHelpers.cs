﻿namespace RuneManager.Domain.Helpers
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Windows.Controls.Primitives;

    using RuneManager.Domain.Extensions;
    using RuneManager.Domain.Enumerations;
    using RuneManager.WPF.Code;

    using WellPoint.Marketing.CampaignTracker.Framework.Extensions;

    public static class ComboBoxHelpers
    {
        public static IList<ComboBoxItem<SetType>> GetRuneSetList()
        {
            return EnumerationExtensions.EnumToList<SetType>().Select(p => new ComboBoxItem<SetType> { Type = p, Label = p.GetDescription() }).OrderBy(st => st.Label).ToList();
        }

        public static IList<int?> GetSlotList()
        {
            var slots = new List<int?>();
            for (var x = 1; x <= 6; x++)
            {
                slots.Add(x);
            }

            return slots;
        }

        public static IList<ComboBoxItem<StatType>> GetStatTypeList()
        {
            return EnumerationExtensions.EnumToList<StatType>().Select(p => new ComboBoxItem<StatType> { Type = p, Label = p.GetDescription() }).ToList();
        }

        public static IList<ComboBoxItem<Element>> GetElementList()
        {
            return EnumerationExtensions.EnumToList<Element>().Select(p => new ComboBoxItem<Element> { Type = p, Label = p.ToString() }).ToList();
        }

        public static IList<ComboBoxItem<SetType>> GetFourSetList()
        {
            return EnumerationExtensions.EnumToList<SetType>().Where(p => p.GetSetSize() == 4).Select(p => new ComboBoxItem<SetType> { Type = p, Label = p.ToString() }).ToList();
        }

        public static IList<ComboBoxItem<SetType>> GetTwoSetList()
        {
            return EnumerationExtensions.EnumToList<SetType>().Where(p => p.GetSetSize() == 2).Select(p => new ComboBoxItem<SetType> { Type = p, Label = p.ToString() }).ToList();
        }
    }
}
