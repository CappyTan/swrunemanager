﻿namespace RuneManager.WPF.Code
{
    using System.Collections.Generic;
    using System.Linq;

    using RuneManager.Domain.Enumerations;

    using WellPoint.Marketing.CampaignTracker.Framework.Extensions;

    public static class CheckBoxHelpers
    {
        public static IList<CheckBoxItem<SetType>> GetRuneSetList()
        {
            return EnumerationExtensions.EnumToList<SetType>().Select(p => new CheckBoxItem<SetType> { IsChecked = false, Label = p.GetDescription(), Type = p }).OrderBy(st => st.Label).ToList();
        }

        public static IList<CheckBoxItem<StatType>> GetStatTypeList()
        {
            return EnumerationExtensions.EnumToList<StatType>().Select(p => new CheckBoxItem<StatType> { IsChecked = false, Label = p.GetDescription(), Type = p }).OrderBy(st => st.Label).ToList();
        }
    }
}
